-- INTRODUCTION --

The GPA Calculator is meant for grade conversions between various schools.
Users will be able to create CGPA calculations, fellowship calculations,
and to retrieve saved CGPA calculations. Each calculation references a grade
scale. Grade scales include a conversion table which calculates the mark
structure for a particular country or institution to the home school's
equivalent grades.

-- INSTRUCTIONS --

To download and install this module, please follow these steps:

1. Enable all modules in the “CGPA Calculator” package on the Modules page.

2. Set permissions appropriately.

3. Visit the CGPA import page at Content Management > Import CGPA content to
import the grade scale and country CSV files. Grade scale CSVs can either have
ID columns or not. If the script does not have an ID column, the only way to
update the data will be through the site’s interface and importing that file
multiple times will result in duplicates.

4. Configure the home page of the site to point to 'cgpa-calculator' or create
a menu item pointing to that path.

5. Add degrees to populate the application. This is an important indicator for
associating a calculation with the correct student. To do so, go to Site
configuration > Administer CGPA settings > CGPA Degrees in the top menu.
All content can be added in English and French.

6. Set the base grades used by the home institution. These will be the grades
to which all transcript grades are converted. This can be set up by going to
Site configuration > Administer CGPA settings > CGPA Grades in the top menu.

7. To enable French localization for the GPA Calculator:

i) Enable the l10n_update module
ii) Go to Administer > Site Building > Translate Interface > Update
and refresh the available updates.
iii) Select your desired translation mode and click "Update translations" at
the bottom of the page.

Currently, the modules are configured to use l10n.gpacalc.ewdev.ca as the
localization server. This is just for evaluation purposes, as Evolving Web
will not be maintaining the localization server indefinitely. The localization
server can be changed in modules' .info files (see the l10n server and l10n
url settings).
