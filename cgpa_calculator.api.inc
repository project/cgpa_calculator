<?php

/**
 * @file
 * Provides API functions for the CGPA Calculator module.
 */


/**
 * Returns all the matching faculty codes that the
 * current user role is allowed to access.
 *
 * Only published nodes are returned.
 */
function cgpa_calculator_get_faculty_list() {
  global $user;
  $fcodes = array();

  foreach ($user->roles as $key => $name) {
    $roles[] = db_result(db_query('
      SELECT rid
      FROM {role}
      WHERE name = "%s"', $name
    ));
  }

  foreach ($roles as $role_id) {
    $fac = db_query('
      SELECT DISTINCT c.field_faculty_code_value AS faculty
      FROM {content_type_cgpa_department} c, {node} n
      WHERE n.vid = c.vid
      AND   n.nid = c.nid
      AND   n.status = 1
      AND   c.field_department_role_value = %d',
      $role_id
    );

    while ($row = db_fetch_array($fac)) {
      $fcodes[$row['faculty']] = $row['faculty'];
    }
  }
  return array_unique($fcodes);
}


/**
 * Returns all the matching department node ids
 * that the current user role allows.
 *
 * Only published nodes are returned.
 */
function cgpa_calculator_get_dept_id() {
  global $user;
  $dcodes = array();

  foreach ($user->roles as $key => $name) {
    $roles[] = db_result(db_query('
      SELECT rid
      FROM {role}
      WHERE name = "%s"', $name
    ));
  }

  foreach ($roles as $key => $role_id) {
    $dept = db_query('
      SELECT DISTINCT c.field_department_name_value AS dept
      FROM {node} n, {content_type_cgpa_department} c
      WHERE n.vid = c.vid
      AND   n.nid = c.nid
      AND   n.status = 1
      AND   c.field_department_role_value = %d', $role_id
    );

    while ($row = db_fetch_array($dept)) {
      $dcodes[$row['dept']] = $row['dept'];
    }
  }

  return array_unique($dcodes);
}


/**
 * Checks if user has view access to the given nid.
 *
 * @param integer $node_id
 *   The ID of the node.
 *
 * @return BOOLEAN
 *   User has access, or doesn't
 */
function cgpa_calculator_user_can_view($node_id) {

  // All the faculties the user has access to based on their roles.
  $fac = array_unique(cgpa_department_get_fac_codes());

  // All the departments the user has access to based on their roles.
  $dept = array_unique(cgpa_department_get_dept_names());

  // Load the node object.
  $n = node_load($node_id);

  foreach ($fac as $value) {
    if ($n->field_faculty[0]['value'] === $value) {
      $fac_exists = TRUE;
    }
  }

  if ($fac_exists) {
    foreach ($dept as $value) {
      if ($n->field_dept[0]['value'] === $value) {
        return TRUE;
      }
    }
  }

  return FALSE;
}
