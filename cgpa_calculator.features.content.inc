<?php

/**
 * @file
 * Features settings for the cgpa_calculator module.
 */

/**
 * Implements hook_content_default_fields().
 */
function cgpa_calculator_content_default_fields() {
  $fields = array();

  // Exported field: field_comments
  $fields['cgpa_calculator-field_comments'] = array(
    'field_name' => 'field_comments',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '51',
      'parent' => 'group_calc_info',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '4',
      'size' => 60,
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Comments',
      'weight' => '51',
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_degree
  $fields['cgpa_calculator-field_degree'] = array(
    'field_name' => 'field_degree',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '44',
      'parent' => 'group_basic',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => 'return array(\'\' => \'\') + cgpa_degree_get_list();
',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Degree',
      'weight' => '44',
      'description' => '',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_degree_desc
  $fields['cgpa_calculator-field_degree_desc'] = array(
    'field_name' => 'field_degree_desc',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '45',
      'parent' => 'group_basic',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Degree Description',
      'weight' => '45',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_dept
  $fields['cgpa_calculator-field_dept'] = array(
    'field_name' => 'field_dept',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '50',
      'parent' => 'group_calc_info',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => 'return cgpa_department_get_dept_names();',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Calculation Department',
      'weight' => '50',
      'description' => 'Select the department the calculation is being done for.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_faculty
  $fields['cgpa_calculator-field_faculty'] = array(
    'field_name' => 'field_faculty',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '49',
      'parent' => 'group_calc_info',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => 'return cgpa_department_get_fac_codes();',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Calculation Faculty',
      'weight' => '49',
      'description' => 'Select the faculty the calculation is being done for.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_grade_scale
  $fields['cgpa_calculator-field_grade_scale'] = array(
    'field_name' => 'field_grade_scale',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '55',
      'parent' => 'group_grade_scale',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'cgpa_gradescale' => 'cgpa_gradescale',
      'cgpa_calculator' => 0,
      'page' => 0,
      'story' => 0,
    ),
    'advanced_view' => 'gradescale_field_options',
    'advanced_view_args' => '',
    'widget' => array(
      'node_link' => array(
        'teaser' => 0,
        'full' => 1,
        'title' => 'Create a new calculation using this grade scale',
        'hover_title' => '',
        'destination' => 'default',
      ),
      'fallback' => 'page_not_found',
      'edit_fallback' => 0,
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'autocomplete_match' => 'contains',
      'size' => 60,
      'label' => 'Grade Scale',
      'weight' => '55',
      'description' => '',
      'type' => 'nodereference_url',
      'module' => 'nodereference_url',
    ),
  );

  // Exported field: field_institution_name
  $fields['cgpa_calculator-field_institution_name'] = array(
    'field_name' => 'field_institution_name',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '46',
      'parent' => 'group_basic',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_institution_name][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Institution Name',
      'weight' => '46',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_student_fname
  $fields['cgpa_calculator-field_student_fname'] = array(
    'field_name' => 'field_student_fname',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '42',
      'parent' => 'group_basic',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Student Firstname',
      'weight' => '42',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_student_id
  $fields['cgpa_calculator-field_student_id'] = array(
    'field_name' => 'field_student_id',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '41',
      'parent' => 'group_basic',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '9',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_student_id][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Student ID',
      'weight' => '41',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_student_lname
  $fields['cgpa_calculator-field_student_lname'] = array(
    'field_name' => 'field_student_lname',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '43',
      'parent' => 'group_basic',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Student Lastname',
      'weight' => '43',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_term
  $fields['cgpa_calculator-field_term'] = array(
    'field_name' => 'field_term',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '48',
      'parent' => 'group_calc_info',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '201109|Fall 2011
201201|Winter 2012
201205|Summer 2012
201209|Fall 2012
201301|Winter 2013
201305|Summer 2013
201309|Fall 2013
201401|Winter 2014
201405|Summer 2014
201409|Fall 2014
201501|Winter 2015
201505|Summer 2015',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => NULL,
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Term',
      'weight' => '48',
      'description' => 'Select the term(s) for which the calculation is being done.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_year_1
  $fields['cgpa_calculator-field_year_1'] = array(
    'field_name' => 'field_year_1',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '47',
      'parent' => 'group_grades',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_calculator_term',
    'required' => '0',
    'multiple' => '4',
    'module' => 'cgpa_calculator',
    'active' => '1',
    'gradescale' => 'field_grade_scale',
    'overall_cgpa_fields' => array(
      'field_year_1' => 'field_year_1',
    ),
    'default_terms' => '2',
    'default_items' => '6',
    'widget' => array(
      'collapse' => 0,
      'default_value' => array(
        '0' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '1' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '2' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '3' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '_update' => 'Update',
      ),
      'default_value_php' => NULL,
      'label' => 'Year 1',
      'weight' => '47',
      'description' => '',
      'type' => 'cgpa_calculator_term_set',
      'module' => 'cgpa_calculator',
    ),
  );

  // Exported field: field_year_2
  $fields['cgpa_calculator-field_year_2'] = array(
    'field_name' => 'field_year_2',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '48',
      'parent' => 'group_grades',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_calculator_term',
    'required' => '0',
    'multiple' => '4',
    'module' => 'cgpa_calculator',
    'active' => '1',
    'gradescale' => 'field_grade_scale',
    'overall_cgpa_fields' => array(
      'field_year_1' => 'field_year_1',
      'field_year_2' => 'field_year_2',
    ),
    'default_terms' => '2',
    'default_items' => '6',
    'widget' => array(
      'collapse' => NULL,
      'default_value' => array(
        '0' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '1' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '2' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '3' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '_update' => 'Update',
      ),
      'default_value_php' => NULL,
      'label' => 'Year 2',
      'weight' => '48',
      'description' => '',
      'type' => 'cgpa_calculator_term_set',
      'module' => 'cgpa_calculator',
    ),
  );

  // Exported field: field_year_3
  $fields['cgpa_calculator-field_year_3'] = array(
    'field_name' => 'field_year_3',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '49',
      'parent' => 'group_grades',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_calculator_term',
    'required' => '0',
    'multiple' => '4',
    'module' => 'cgpa_calculator',
    'active' => '1',
    'gradescale' => 'field_grade_scale',
    'overall_cgpa_fields' => array(
      'field_year_1' => 'field_year_1',
      'field_year_2' => 'field_year_2',
      'field_year_3' => 'field_year_3',
    ),
    'default_terms' => '2',
    'default_items' => '6',
    'widget' => array(
      'collapse' => NULL,
      'default_value' => array(
        '0' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '1' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '2' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '3' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '_update' => 'Update',
      ),
      'default_value_php' => NULL,
      'label' => 'Year 3',
      'weight' => '49',
      'description' => '',
      'type' => 'cgpa_calculator_term_set',
      'module' => 'cgpa_calculator',
    ),
  );

  // Exported field: field_year_4
  $fields['cgpa_calculator-field_year_4'] = array(
    'field_name' => 'field_year_4',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '50',
      'parent' => 'group_grades',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_calculator_term',
    'required' => '0',
    'multiple' => '4',
    'module' => 'cgpa_calculator',
    'active' => '1',
    'gradescale' => 'field_grade_scale',
    'overall_cgpa_fields' => array(
      'field_year_1' => 'field_year_1',
      'field_year_2' => 'field_year_2',
      'field_year_3' => 'field_year_3',
      'field_year_4' => 'field_year_4',
    ),
    'default_terms' => '2',
    'default_items' => '6',
    'widget' => array(
      'collapse' => 1,
      'default_value' => array(
        '0' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '1' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '2' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '3' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '_update' => 'Update',
      ),
      'default_value_php' => NULL,
      'label' => 'Year 4',
      'weight' => '50',
      'description' => '',
      'type' => 'cgpa_calculator_term_set',
      'module' => 'cgpa_calculator',
    ),
  );

  // Exported field: field_year_5
  $fields['cgpa_calculator-field_year_5'] = array(
    'field_name' => 'field_year_5',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '51',
      'parent' => 'group_grades',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_calculator_term',
    'required' => '0',
    'multiple' => '4',
    'module' => 'cgpa_calculator',
    'active' => '1',
    'gradescale' => 'field_grade_scale',
    'overall_cgpa_fields' => array(
      'field_year_1' => 'field_year_1',
      'field_year_2' => 'field_year_2',
      'field_year_3' => 'field_year_3',
      'field_year_4' => 'field_year_4',
      'field_year_5' => 'field_year_5',
    ),
    'default_terms' => '2',
    'default_items' => '6',
    'widget' => array(
      'collapse' => 1,
      'default_value' => array(
        '0' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '1' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '2' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '3' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '_update' => 'Update',
      ),
      'default_value_php' => NULL,
      'label' => 'Year 5',
      'weight' => '51',
      'description' => '',
      'type' => 'cgpa_calculator_term_set',
      'module' => 'cgpa_calculator',
    ),
  );

  // Exported field: field_year_6
  $fields['cgpa_calculator-field_year_6'] = array(
    'field_name' => 'field_year_6',
    'type_name' => 'cgpa_calculator',
    'display_settings' => array(
      'weight' => '52',
      'parent' => 'group_grades',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'onetable',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_calculator_term',
    'required' => '0',
    'multiple' => '4',
    'module' => 'cgpa_calculator',
    'active' => '1',
    'gradescale' => 'field_grade_scale',
    'overall_cgpa_fields' => array(
      'field_year_1' => 'field_year_1',
      'field_year_2' => 'field_year_2',
      'field_year_3' => 'field_year_3',
      'field_year_4' => 'field_year_4',
      'field_year_5' => 'field_year_5',
      'field_year_6' => 'field_year_6',
    ),
    'default_terms' => '2',
    'default_items' => '6',
    'widget' => array(
      'collapse' => 1,
      'default_value' => array(
        '0' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '1' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '2' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '3' => array(
          'value' => array(
            '0' => array(
              'mark' => '',
              'credits' => '',
            ),
            '_add_more' => '+',
          ),
        ),
        '_update' => 'Update',
      ),
      'default_value_php' => NULL,
      'label' => 'Year 6',
      'weight' => '52',
      'description' => '',
      'type' => 'cgpa_calculator_term_set',
      'module' => 'cgpa_calculator',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Calculation Department');
  t('Calculation Faculty');
  t('Comments');
  t('Degree');
  t('Degree Description');
  t('Grade Scale');
  t('Institution Name');
  t('Student Firstname');
  t('Student ID');
  t('Student Lastname');
  t('Term');
  t('Year 1');
  t('Year 2');
  t('Year 3');
  t('Year 4');
  t('Year 5');
  t('Year 6');

  return $fields;
}
