<?php

/**
* @file
* Fieldgroups for the CCK in the cgpa_calculator module.
*/

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function cgpa_calculator_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_basic
  $groups['cgpa_calculator-group_basic'] = array(
    'group_type' => 'standard',
    'type_name' => 'cgpa_calculator',
    'group_name' => 'group_basic',
    'label' => 'Transcript Information',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'weight' => '36',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '36',
    'fields' => array(
      '0' => 'field_student_id',
      '1' => 'field_student_fname',
      '2' => 'field_student_lname',
      '3' => 'field_degree',
      '4' => 'field_degree_desc',
      '5' => 'field_institution_name',
    ),
  );

  // Exported group: group_calc_info
  $groups['cgpa_calculator-group_calc_info'] = array(
    'group_type' => 'standard',
    'type_name' => 'cgpa_calculator',
    'group_name' => 'group_calc_info',
    'label' => 'Calculation Information',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'weight' => '39',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '39',
    'fields' => array(
      '0' => 'field_term',
      '1' => 'field_faculty',
      '2' => 'field_dept',
      '3' => 'field_comments',
    ),
  );

  // Exported group: group_grade_scale
  $groups['cgpa_calculator-group_grade_scale'] = array(
    'group_type' => 'standard',
    'type_name' => 'cgpa_calculator',
    'group_name' => 'group_grade_scale',
    'label' => 'Grade Scale',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'weight' => '37',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '37',
    'fields' => array(
      '0' => 'field_grade_scale',
    ),
  );

  // Exported group: group_grades
  $groups['cgpa_calculator-group_grades'] = array(
    'group_type' => 'standard',
    'type_name' => 'cgpa_calculator',
    'group_name' => 'group_grades',
    'label' => 'Grades',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'weight' => '38',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '38',
    'fields' => array(
      '0' => 'field_year_1',
      '1' => 'field_year_2',
      '2' => 'field_year_3',
      '3' => 'field_year_4',
      '4' => 'field_year_5',
      '5' => 'field_year_6',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Calculation Information');
  t('Grade Scale');
  t('Grades');
  t('Transcript Information');

  return $groups;
}
