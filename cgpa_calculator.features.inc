<?php

/**
* @file
* Features settings for the cgpa_calculator module.
*/


/**
 * Implementation of hook_ctools_plugin_api().
 */
function cgpa_calculator_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function cgpa_calculator_node_info() {
  $items = array(
    'cgpa_calculator' => array(
      'name' => t('CGPA Calculation'),
      'module' => 'features',
      'description' => t('This is the main GPA calculator form. Allows users to convert the student\'s transcript marks to the local school grade and derive the school grade point. It then calculates the GPA for each year and the CGPA.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function cgpa_calculator_views_api() {
  return array(
    'api' => '3.0',
  );
}
