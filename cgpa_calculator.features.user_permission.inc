<?php

/**
 * @file
 * Permissions for the cgpa_calculator module.
 */


/**
 * Implementation of hook_user_default_permissions().
 */
function cgpa_calculator_user_default_permissions() {
  $permissions = array();

  // Exported permission: access admin views
  $permissions['access admin views'] = array(
    'name' => 'access admin views',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa gps users',
    ),
  );

  // Exported permission: access content
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: access data entry views
  $permissions['access data entry views'] = array(
    'name' => 'access data entry views',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: administer cgpa landing page text
  $permissions['administer cgpa landing page text'] = array(
    'name' => 'administer cgpa landing page text',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: administer cgpa_gradescale grade list
  $permissions['administer cgpa_gradescale grade list'] = array(
    'name' => 'administer cgpa_gradescale grade list',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_country create
  $permissions['cgpa_country create'] = array(
    'name' => 'cgpa_country create',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_country delete
  $permissions['cgpa_country delete'] = array(
    'name' => 'cgpa_country delete',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_country read
  $permissions['cgpa_country read'] = array(
    'name' => 'cgpa_country read',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_country update
  $permissions['cgpa_country update'] = array(
    'name' => 'cgpa_country update',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_degree create
  $permissions['cgpa_degree create'] = array(
    'name' => 'cgpa_degree create',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_degree delete
  $permissions['cgpa_degree delete'] = array(
    'name' => 'cgpa_degree delete',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_degree read
  $permissions['cgpa_degree read'] = array(
    'name' => 'cgpa_degree read',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: cgpa_degree update
  $permissions['cgpa_degree update'] = array(
    'name' => 'cgpa_degree update',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: create cgpa_calculator content
  $permissions['create cgpa_calculator content'] = array(
    'name' => 'create cgpa_calculator content',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: create cgpa_department content
  $permissions['create cgpa_department content'] = array(
    'name' => 'create cgpa_department content',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: create cgpa_gradescale content
  $permissions['create cgpa_gradescale content'] = array(
    'name' => 'create cgpa_gradescale content',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: delete any cgpa_calculator content
  $permissions['delete any cgpa_calculator content'] = array(
    'name' => 'delete any cgpa_calculator content',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa gps users',
    ),
  );

  // Exported permission: delete any cgpa_department content
  $permissions['delete any cgpa_department content'] = array(
    'name' => 'delete any cgpa_department content',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: delete any cgpa_gradescale content
  $permissions['delete any cgpa_gradescale content'] = array(
    'name' => 'delete any cgpa_gradescale content',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: delete own cgpa_calculator content
  $permissions['delete own cgpa_calculator content'] = array(
    'name' => 'delete own cgpa_calculator content',
    'roles' => array(
      '0' => 'gpa data entry',
    ),
  );

  // Exported permission: delete own cgpa_department content
  $permissions['delete own cgpa_department content'] = array(
    'name' => 'delete own cgpa_department content',
    'roles' => array(),
  );

  // Exported permission: delete own cgpa_gradescale content
  $permissions['delete own cgpa_gradescale content'] = array(
    'name' => 'delete own cgpa_gradescale content',
    'roles' => array(),
  );

  // Exported permission: edit any cgpa_calculator content
  $permissions['edit any cgpa_calculator content'] = array(
    'name' => 'edit any cgpa_calculator content',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit any cgpa_department content
  $permissions['edit any cgpa_department content'] = array(
    'name' => 'edit any cgpa_department content',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit any cgpa_gradescale content
  $permissions['edit any cgpa_gradescale content'] = array(
    'name' => 'edit any cgpa_gradescale content',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_comments
  $permissions['edit field_comments'] = array(
    'name' => 'edit field_comments',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_degree
  $permissions['edit field_degree'] = array(
    'name' => 'edit field_degree',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_degree_desc
  $permissions['edit field_degree_desc'] = array(
    'name' => 'edit field_degree_desc',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_department_code
  $permissions['edit field_department_code'] = array(
    'name' => 'edit field_department_code',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_department_name
  $permissions['edit field_department_name'] = array(
    'name' => 'edit field_department_name',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_department_role
  $permissions['edit field_department_role'] = array(
    'name' => 'edit field_department_role',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_dept
  $permissions['edit field_dept'] = array(
    'name' => 'edit field_dept',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_faculty
  $permissions['edit field_faculty'] = array(
    'name' => 'edit field_faculty',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_faculty_code
  $permissions['edit field_faculty_code'] = array(
    'name' => 'edit field_faculty_code',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_grade_scale
  $permissions['edit field_grade_scale'] = array(
    'name' => 'edit field_grade_scale',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_institution_name
  $permissions['edit field_institution_name'] = array(
    'name' => 'edit field_institution_name',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_scale_country
  $permissions['edit field_scale_country'] = array(
    'name' => 'edit field_scale_country',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_scale_id
  $permissions['edit field_scale_id'] = array(
    'name' => 'edit field_scale_id',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_scale_max
  $permissions['edit field_scale_max'] = array(
    'name' => 'edit field_scale_max',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_scale_name
  $permissions['edit field_scale_name'] = array(
    'name' => 'edit field_scale_name',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_scale_scale
  $permissions['edit field_scale_scale'] = array(
    'name' => 'edit field_scale_scale',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_scale_type
  $permissions['edit field_scale_type'] = array(
    'name' => 'edit field_scale_type',
    'roles' => array(
      '0' => 'gpa admin',
    ),
  );

  // Exported permission: edit field_student_fname
  $permissions['edit field_student_fname'] = array(
    'name' => 'edit field_student_fname',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_student_id
  $permissions['edit field_student_id'] = array(
    'name' => 'edit field_student_id',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_student_lname
  $permissions['edit field_student_lname'] = array(
    'name' => 'edit field_student_lname',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_term
  $permissions['edit field_term'] = array(
    'name' => 'edit field_term',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_year_1
  $permissions['edit field_year_1'] = array(
    'name' => 'edit field_year_1',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_year_2
  $permissions['edit field_year_2'] = array(
    'name' => 'edit field_year_2',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_year_3
  $permissions['edit field_year_3'] = array(
    'name' => 'edit field_year_3',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_year_4
  $permissions['edit field_year_4'] = array(
    'name' => 'edit field_year_4',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_year_5
  $permissions['edit field_year_5'] = array(
    'name' => 'edit field_year_5',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit field_year_6
  $permissions['edit field_year_6'] = array(
    'name' => 'edit field_year_6',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: edit own cgpa_calculator content
  $permissions['edit own cgpa_calculator content'] = array(
    'name' => 'edit own cgpa_calculator content',
    'roles' => array(),
  );

  // Exported permission: edit own cgpa_department content
  $permissions['edit own cgpa_department content'] = array(
    'name' => 'edit own cgpa_department content',
    'roles' => array(),
  );

  // Exported permission: edit own cgpa_gradescale content
  $permissions['edit own cgpa_gradescale content'] = array(
    'name' => 'edit own cgpa_gradescale content',
    'roles' => array(),
  );

  // Exported permission: perform fellowship calculations
  $permissions['perform fellowship calculations'] = array(
    'name' => 'perform fellowship calculations',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: use institution autocomplete
  $permissions['use institution autocomplete'] = array(
    'name' => 'use institution autocomplete',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_comments
  $permissions['view field_comments'] = array(
    'name' => 'view field_comments',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_degree
  $permissions['view field_degree'] = array(
    'name' => 'view field_degree',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_degree_desc
  $permissions['view field_degree_desc'] = array(
    'name' => 'view field_degree_desc',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_department_code
  $permissions['view field_department_code'] = array(
    'name' => 'view field_department_code',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_department_name
  $permissions['view field_department_name'] = array(
    'name' => 'view field_department_name',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_department_role
  $permissions['view field_department_role'] = array(
    'name' => 'view field_department_role',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_dept
  $permissions['view field_dept'] = array(
    'name' => 'view field_dept',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_faculty
  $permissions['view field_faculty'] = array(
    'name' => 'view field_faculty',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_faculty_code
  $permissions['view field_faculty_code'] = array(
    'name' => 'view field_faculty_code',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_grade_scale
  $permissions['view field_grade_scale'] = array(
    'name' => 'view field_grade_scale',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_institution_name
  $permissions['view field_institution_name'] = array(
    'name' => 'view field_institution_name',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_scale_country
  $permissions['view field_scale_country'] = array(
    'name' => 'view field_scale_country',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_scale_id
  $permissions['view field_scale_id'] = array(
    'name' => 'view field_scale_id',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_scale_max
  $permissions['view field_scale_max'] = array(
    'name' => 'view field_scale_max',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_scale_name
  $permissions['view field_scale_name'] = array(
    'name' => 'view field_scale_name',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_scale_scale
  $permissions['view field_scale_scale'] = array(
    'name' => 'view field_scale_scale',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_scale_type
  $permissions['view field_scale_type'] = array(
    'name' => 'view field_scale_type',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_student_fname
  $permissions['view field_student_fname'] = array(
    'name' => 'view field_student_fname',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_student_id
  $permissions['view field_student_id'] = array(
    'name' => 'view field_student_id',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_student_lname
  $permissions['view field_student_lname'] = array(
    'name' => 'view field_student_lname',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_term
  $permissions['view field_term'] = array(
    'name' => 'view field_term',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_year_1
  $permissions['view field_year_1'] = array(
    'name' => 'view field_year_1',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_year_2
  $permissions['view field_year_2'] = array(
    'name' => 'view field_year_2',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_year_3
  $permissions['view field_year_3'] = array(
    'name' => 'view field_year_3',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_year_4
  $permissions['view field_year_4'] = array(
    'name' => 'view field_year_4',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_year_5
  $permissions['view field_year_5'] = array(
    'name' => 'view field_year_5',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  // Exported permission: view field_year_6
  $permissions['view field_year_6'] = array(
    'name' => 'view field_year_6',
    'roles' => array(
      '0' => 'gpa ES',
      '1' => 'gpa admin',
      '2' => 'gpa data entry',
      '3' => 'gpa gps users',
    ),
  );

  return $permissions;
}
