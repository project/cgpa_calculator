<?php

/**
 * @file
 * Roles for the cgpa_calculator module.
 */

/**
 * Implementation of hook_user_default_roles().
 */
function cgpa_calculator_user_default_roles() {
  $roles = array();

  // Exported role: gpa ES
  $roles['gpa ES'] = array(
    'name' => 'gpa ES',
  );

  // Exported role: gpa admin
  $roles['gpa admin'] = array(
    'name' => 'gpa admin',
  );

  // Exported role: gpa data entry
  $roles['gpa data entry'] = array(
    'name' => 'gpa data entry',
  );

  // Exported role: gpa gps users
  $roles['gpa gps users'] = array(
    'name' => 'gpa gps users',
  );

  return $roles;
}

