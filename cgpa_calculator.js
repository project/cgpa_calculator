
(function($) {

  var collapsed = {};

  /**
   * Behavior on our custom field to erase the calculated values when someone changes the value of the test fields.
   */
  Drupal.behaviors.cgpaCalculatorTermSet = function(context) {
    $('.cgpa-calculator-term-set-table:not(.cgpa-calculator-term-set-table-processed)', context)
      .addClass('cgpa-calculator-term-set-table-processed')
      .each(function() {

        // If the input changes, reset the row's calculated values.
        $(this).find('tr:has(td) input[type="text"]').bind('change', function() {
          $(this).parents('tr:first').find('.cgpa-calculator-term-set-info').text('');
        });


        /**
         * Tweak fieldset expansion so if we replace it with AHAH, it will expand again on view.
         */
        (function() {
          var fieldset = $(this).parents('fieldset:first');
          var col = fieldset.is('.collapsed');
          var id = fieldset.parents('div:first').attr('id');

          if (typeof collapsed[id] === 'undefined') {
            collapsed[id] = col;
          }
          else if (!collapsed[id] && col) {
            Drupal.toggleFieldset(fieldset);
          }
        })();
      });
  };

  /**
   * Behavior to automatically submit the exposed form when an option is selected.
   */
  Drupal.behaviors.cgpaCalculatorAdmissionsView = function(context) {
    $('#views-exposed-form-cgpa-calculator-admissions-view-page-1:not(.cgpa-calculator-admissions-view-processed)', context)
      .addClass('cgpa-calculator-admissions-view-processed')
      .each(function() {
        var form = $(this);

        // Watch the change event, and submit
        form.find('select').change(function() {
          form.submit();
        });

        // Hide submit button
        form.find('.form-submit').hide();
      });
  };

  /**
   * Behavior to capture 'enter' key presses, to force them to press proper submit button.
   * Otherwise pressing enter will trigger first submit button in form, adding items to field.
   */
  Drupal.behaviors.cgpaCalculatorSubmitEnter = function(context) {
    $('.cgpa-calculator-node-form:not(.cgpa-calculator-submit-enter-processed)', context)
      .addClass('cgpa-calculator-submit-enter-processed')
      .keypress(function(e) {
        if (e.which == 13) {
          e.preventDefault();
          $(this).find('#edit-submit').click();
        }
      });
  };

 })(jQuery);
