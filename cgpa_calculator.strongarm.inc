<?php

/**
 * @file
 * Strongarm settings for the cgpa_calculator module.
 */


/**
 * Implementation of hook_strongarm().
 */
function cgpa_calculator_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_cgpa_calculator';
  $strongarm->value = '1';
  $export['ant_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_cgpa_calculator';
  $strongarm->value = '[field_student_fname-formatted] [field_student_lname-formatted] / [field_student_id-formatted] / [field_degree-formatted]';
  $export['ant_pattern_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_cgpa_calculator';
  $strongarm->value = 0;
  $export['ant_php_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_cgpa_calculator';
  $strongarm->value = array(
    'title' => '33',
    'revision_information' => '45',
    'author' => '46',
    'options' => '47',
    'menu' => '44',
    'path' => '40',
    'custom_breadcrumbs' => '42',
    'path_redirect' => '43',
    'print' => '41',
    'cgpa_summary' => '35',
    'transcript_metadata' => '34',
  );
  $export['content_extra_weights_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_cgpa_calculator';
  $strongarm->value = 1;
  $export['enable_revisions_page_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_cgpa_calculator';
  $strongarm->value = '0';
  $export['language_content_type_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_cgpa_calculator';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_cgpa_calculator';
  $strongarm->value = 0;
  $export['show_diff_inline_cgpa_calculator'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_cgpa_calculator';
  $strongarm->value = 1;
  $export['show_preview_changes_cgpa_calculator'] = $strongarm;

  return $export;
}
