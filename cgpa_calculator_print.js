(function($) {
    Drupal.behaviors.cgpaCalculatorFixPrint = function(context) {
        var p = $('#cgpa-calculator-view-print');
        //unbind other events if they exist...
        p.unbind();
        //removes inline onclick function (unbind wont work for this)
        p.removeAttr('onclick');
        p.click(function(){
            //gets all the fields, will be empty on second click, 
            // so no need to check times clicked...
            var f = $('fieldset');
            //gets all their content and puts it after, not quite 
            //sure why they're empty after that...
            f.each(function(i){var o = $(this); var c =o.children(); o.after(c); });
            //removes the fields
            f.remove();
            //does print anyway...
            window.print();
        });
    };
})(jQuery);
