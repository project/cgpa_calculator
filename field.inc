<?php
/**
 * @file
 * Defines all CCK related hooks for specifing field data.
 */

/**
 * Implements hook_field_info().
 */
function cgpa_calculator_field_info() {
  $fields['cgpa_calculator_term'] = array(
    'label'       => t('CGPA Calculator Term'),
    'description' => t('Each value represents the grades and credits for the courses of a given term.'),
  );
  return $fields;
}

/**
 * Implements hook_widget_info().
 */
function cgpa_calculator_widget_info() {
  $widgets['cgpa_calculator_term_set'] = array(
    'label'           => t('Term Set'),
    'field types'     => array('cgpa_calculator_term'),
    'multiple values' => CONTENT_HANDLE_MODULE,
    'callbacks'       => array('default value' => CONTENT_CALLBACK_NONE),
  );
  return $widgets;
}

/**
 * Implements hook_field_formatter_info().
 */
function cgpa_calculator_field_formatter_info() {
  $formatters['onetable'] = array(
    'label'           => t('Default'),
    'field types'     => array('cgpa_calculator_term'),
    'multiple values' => CONTENT_HANDLE_MODULE,
  );
  return $formatters;
}

/**
 * Implements hook_field_settings().
 *
 * @param string $op
 *   'form'
 *   'validate'
 *   'save'
 *   'database columns'
 *   'filters'
 *   'views data'
 */
function cgpa_calculator_field_settings($op, $field) {

  if ($field['type'] == 'cgpa_calculator_term') {
    switch ($op) {
      case 'database columns':
        return array(
          'termid' => array(
            'type'      => 'int',
            'not null'  => FALSE,
          ),
        );

      case 'form':
        $form = array();
        $type = content_types('cgpa_calculator');
        $nodereferences = array();
        $cgpa_calculator_term_fields = array();
        foreach ($type['fields'] as $field_name => $info) {
          if ($info['type'] == 'nodereference') {
            $nodereferences[$field_name] = $info['widget']['label'];
          }
          elseif ($info['type'] == 'cgpa_calculator_term') {
            $cgpa_calculator_term_fields[$field_name]
              = $info['widget']['label'];
          }
        }
        $form['gradescale'] = array(
          '#type'           => 'select',
          '#title'          => t('Gradescale Field'),
          '#description'    => t('Which field references the gradescale to use for this calculation?'),
          '#options'        => $nodereferences,
          '#default_value'  => empty($field['gradescale']) ? '' : $field['gradescale'],
        );
        $form['overall_cgpa_fields'] = array(
          '#type'           => 'select',
          '#multiple'       => TRUE,
          '#title'          => t('Overall CGPA Fields'),
          '#description'    => t('Which fields should be used to compute the overall CGPA?'),
          '#options'        => $cgpa_calculator_term_fields,
          '#default_value'  => empty($field['overall_cgpa_fields']) ? '' : $field['overall_cgpa_fields'],
        );
        $form['default_terms'] = array(
          '#type'           => 'textfield',
          '#title'          => t('Prefill Terms'),
          '#description'    => t('How many terms should be prefilled with items?'),
          '#default_value'  => isset($field['default_terms']) ? $field['default_terms'] : '',
          '#size'           => 5,
        );
        $form['default_items'] = array(
          '#type'           => 'textfield',
          '#title'          => t('Prefill Items'),
          '#description'    => t('How many items should be prefilled for each prefillable term?'),
          '#default_value'  => isset($field['default_items']) ? $field['default_items'] : '',
          '#size'           => 5,
        );
        return $form;
      case 'save':
        return array(
          'gradescale',
          'overall_cgpa_fields',
          'default_terms',
          'default_items',
        );
    }
  }
}

/**
 * Implements hook_widget_settings().
 *
 * @param string $op
 *   'form'
 *   'validate'
 *   'save'
 */
function cgpa_calculator_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      return array(
        'collapse' => array(
          '#type'           => 'checkbox',
          '#title'          => t('Collapse the field by default'),
          '#description'    => t('Hide the field in a collapsed fieldset'),
          '#default_value'  => !empty($widget['collapse']),
        ),
      );
    case 'save':
      return array('collapse');
  }
}

/**
 * Implements hook_content_is_empty().
 *
 * @param string $item
 *   The item to check for emptiness.
 *
 * @param array $field
 *   The field that this item provides values for.
 */
function cgpa_calculator_content_is_empty($item, $field) {
  if ($field['type'] == 'cgpa_calculator_term') {
    // Check if it is a Year value, or has values.
    return !is_numeric($item) && empty($item['value']);
  }
}

/**
 * Implements hook_field().
 *
 * @param string $op
 *   'validate'
 *   'presave'
 *   'insert'
 *   'update'
 *   'delete'
 *   'delete revision'
 *   'sanitize'
 *   'prepare translation'
 *
 */
function cgpa_calculator_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($field['type']) {
    case 'cgpa_calculator_term':
      switch ($op) {
        case 'load':
          cgpa_calculator_load_marks($items);
          return array($field['field_name'] => $items);
        case 'presave':
          cgpa_calculator_strip_buttons($items);
          break;
        case 'insert':
        case 'update':
          cgpa_calculator_save_marks($items);
          break;
      }
      break;
  }
}

/**
 * Implements hook_widget().
 *
 */
function cgpa_calculator_widget(
  &$form, &$form_state, $field, $items, $delta = 0) {

  $element = array();
  switch ($field['widget']['type']) {
    case 'cgpa_calculator_term_set':
      $terms_prefill
        = isset($field['default_terms']) ? $field['default_terms'] : 0;
      $items_prefill
        = isset($field['default_items']) ? $field['default_items'] : 0;

      cgpa_calculator_strip_buttons($items);

      for ($i = 0; $i < $terms_prefill; $i++) {
        if (!isset($items[$i]['value'])) {
          $items[$i]['value'] = array();
        }

        $count = count($items[$i]['value']);
        if ($count < $items_prefill) {
          for ($j = 0; $j < ($items_prefill - $count); $j++) {
            $items[$i]['value'][] = array();
          }
        }
      }

      // This adds more items when you press the '+' button on the term.
      // The 'cgpa_calculator_increment' variable is set in the js
      // add_more callback.
      if (isset($form_state['cgpa_calculator_increment'][$field['field_name']]))
      {
        $i = $form_state['cgpa_calculator_increment'][$field['field_name']];
        for ($j = 0; $j < 3; $j++) {
          $items[$i]['value'][] = array();
        }
      }

      $element = array(
        '#title'         => check_plain($field['widget']['label']),
        '#description'   => content_filter_xss($field['widget']['description']),
        '#type'          => 'cgpa_calculator_term_set',
        '#default_value' => $items,
      );
      break;
  }
  return $element;
}

/**
 * Helper function to go through terms and remove all '_update'
 * and '_add_more' values.
 */
function cgpa_calculator_strip_buttons(&$items) {
  unset($items['_update']);
  foreach ($items as $tid => $term) {
    if (is_numeric($tid)) {
      unset($items[$tid]['value']['_add_more']);
    }
  }
}

/**
 * Load function for course values.
 *
 * @see hook_field
 */
function cgpa_calculator_load_marks(&$items) {

  // Aggregate all term ids.
  $term_ids = array();
  foreach ($items as $d => $item) {
    if (is_numeric($d) && !isset($item['value']) && isset($item['termid'])) {
      $term_ids[$d] = $item['termid'];
    }
  }

  // Load values for all terms.
  if (!empty($term_ids)) {
    $results = db_query("
      SELECT year, term, tmid, gct.tid, mark, credits
      FROM {cgpa_calculator_term_marks} gctm
      JOIN {cgpa_calculator_terms} gct ON gct.tid = gctm.tid
      WHERE gctm.tid IN (" . db_placeholders($term_ids, 'int') . ")",
      $term_ids);

    $year = '';
    $deltas = array_flip($term_ids);
    while ($row = db_fetch_object($results)) {
      $grades[$deltas[$row->tid]][$row->tmid] = (array) $row;
      $items[$deltas[$row->tid]]['value'][$row->tmid] = (array) $row;
      $year = $row->year;
    }
    if (!empty($items)) {
      $items['year'] = $year;
    }
  }
}

/**
 * Save function for course values.
 *
 * @see hook_field
 */
function cgpa_calculator_save_marks(&$items) {

  // Retrieve year and term.
  $year = '';
  if (isset($items[0]) && !is_array($items[0])) {
    $year = $items[0];
    unset($items[0]);
  }

  foreach ($items as $delta => &$item) {
    if (!is_numeric($delta)) {
      continue;
    }

    if (empty($item['value'])) {
      $item['value'] = array();
    }

    if (empty($item['termid']) && !empty($item['value'])) {
      db_query("INSERT INTO {cgpa_calculator_terms} (year) VALUES ('%s')", array($year));
      $item['termid'] = db_last_insert_id('cgpa_calculator_terms', 'tid');
    }
    elseif (!empty($item['termid'])) {
      db_query("DELETE FROM {cgpa_calculator_term_marks} WHERE tid=%d", $item['termid']);
    }

    foreach ($item['value'] as $i => $value) {
      $value += array('mark' => '', 'credits' => '');

      if ($value['mark'] !== '') {
        db_query("INSERT INTO {cgpa_calculator_term_marks}(tid,mark,credits) VALUES (%d, '%s', %f)",
                  $item['termid'], $value['mark'], $value['credits']);
      }
    }
  }
}
