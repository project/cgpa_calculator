<?php
/**
 * @file
 * Auto Admin schemas for cgpa_country module.
 */

/**
 * Implements hook_autoadmin_schema().
 */
function cgpa_country_autoadmin_schema() {
  $schema['cgpa_country'] = array(
    'alias' => 'name_english',
    'path' => 'admin/settings/cgpa/countries',
    'description' => t('Table listing countries.'),
    'title' => t('CGPA Country'),
    'title_plural' => t('CGPA Countries'),
    'fields' => array(
      'id' => array(
        'autoadmin_type' => 'primary_key',
        'title' => t('Country ID'),
        'title_plural' => t('Countries IDs'),
      ),
      'code' => array(
        'autoadmin_type' => 'varchar',
        'title' => t('Country Code'),
        'title_plural' => t('Country codes'),
      ),
      'name_english' => array(
        'autoadmin_type' => 'varchar',
        'title' => t('Country Name in English'),
        'title_plural' => t('Country Names in English'),
      ),
      'name_french' => array(
        'autoadmin_type' => 'varchar',
        'title' => t('Country Name in French'),
        'title_plural' => t('Country Names in French'),
      ),
    ),
  );

  return $schema;
}
