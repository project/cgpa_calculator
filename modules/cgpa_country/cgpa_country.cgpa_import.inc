<?php
/**
 * @file
 * Provides class for managing country imports.
 */

class CountryImporter extends Importer {
  // CSV column numbers.
  const COL_CODE = 0;
  const COL_EN = 1;
  const COL_FR = 2;

  protected $countries;

  /**
   * Constructor
   */
  public function __construct($filename) {
    parent::__construct($filename);
    $this->countries = cgpa_country_get_countries();
  }

  /**
   * Import a row.
   *
   * @return: FALSE when last row parsed, TRUE otherwise.
   */
  protected function parse() {
    $row = $this->csv->getRow();
    if (empty($row)) {
      return FALSE;
    }
    $names = array(
      'en' => $row[self::COL_EN],
      'fr' => $row[self::COL_FR],
    );
    $code = $row[self::COL_CODE];
    $country = $this->countries->getCountryByCode($code);
    if (!$country) {
      $this->countries->createCountry($names, $code);
    }
    return TRUE;
  }
}
