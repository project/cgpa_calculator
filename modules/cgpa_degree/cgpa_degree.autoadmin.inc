<?php
/**
 * @file
 * Auto Admin schemas for cgpa_degree module.
 */

/**
 * Implements hook_autoadmin_schema().
 */
function cgpa_degree_autoadmin_schema() {
  $schema['cgpa_degree'] = array(
    'alias' => 'name_english',
    'path' => 'admin/settings/cgpa/degrees',
    'description' => t('Table containing all the different allowed types of degrees.'),
    'title' => t('CGPA Degree'),
    'title_plural' => t('CGPA Degrees'),
    'fields' => array(
      'degree_id' => array(
        'autoadmin_type' => 'primary_key',
        'title' => t('Degree ID'),
        'title_plural' => t('Degree IDs'),
      ),
      'name_english' => array(
        'autoadmin_type' => 'varchar',
        'title' => t('Degree Name in English'),
        'title_plural' => t('Degree Names in English'),
      ),
      'name_french' => array(
        'autoadmin_type' => 'varchar',
        'title' => t('Degree Name in French'),
        'title_plural' => t('Degree Names in French'),
      ),
    ),
  );

  return $schema;
}
