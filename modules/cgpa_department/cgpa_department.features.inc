<?php

/**
 * @file
 * Features settings for the cgpa_department module.
 */

/**
 * Implementation of hook_cgpa_features_fixes_missed_views_translations().
 */
function cgpa_department_cgpa_features_fixes_missed_views_translations() {
  // Translatables
  // Included for use with string extractors like potx.
  t('Administer Department List');
  t('CGPA Departments');
  t('Department Name');
  t('Enabled');
  t('Faculty Code');
  t('Modify department list for the CGPA calculator.');
  t('Published');

}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function cgpa_department_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function cgpa_department_node_info() {
  $items = array(
    'cgpa_department' => array(
      'name' => t('CGPA Department'),
      'module' => 'features',
      'description' => t('Department for the which the CGPA is being calculated and associated user role.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function cgpa_department_views_api() {
  return array(
    'api' => '2',
  );
}
