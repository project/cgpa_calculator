<?php

/**
 * @file
 * Strongarm settings for the cgpa_department module.
 */

/**
 * Implementation of hook_strongarm().
 */
function cgpa_department_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_cgpa_department';
  $strongarm->value = '1';
  $export['ant_cgpa_department'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_cgpa_department';
  $strongarm->value = '[field_department_code-formatted] - [field_department_name-formatted] - [field_department_role-formatted]';
  $export['ant_pattern_cgpa_department'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_cgpa_department';
  $strongarm->value = 0;
  $export['ant_php_cgpa_department'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_cgpa_department';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '1',
    'author' => '2',
    'options' => '3',
    'menu' => '0',
    'path' => '7',
    'custom_breadcrumbs' => '6',
    'path_redirect' => '4',
    'print' => '5',
  );
  $export['content_extra_weights_cgpa_department'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_cgpa_department';
  $strongarm->value = '0';
  $export['language_content_type_cgpa_department'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_cgpa_department';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_cgpa_department'] = $strongarm;

  return $export;
}
