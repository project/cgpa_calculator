<?php

/**
 * @file
 * Views for the cgpa_department module.
 */
 
/**
 * Implementation of hook_views_default_views().
 */
function cgpa_department_views_default_views() {
  $views = array();

  // Exported view: cgpa_department_view
  $view = new view;
  $view->name = 'cgpa_department_view';
  $view->description = 'Add, update and delete departments.';
  $view->tag = 'cgpa';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Department View', 'default');
  $handler->override_option('fields', array(
    'field_faculty_code_value' => array(
      'label' => 'Faculty Code',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_faculty_code_value',
      'table' => 'node_data_field_faculty_code',
      'field' => 'field_faculty_code_value',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'field_department_code_value' => array(
      'id' => 'field_department_code_value',
      'table' => 'node_data_field_department_code',
      'field' => 'field_department_code_value',
    ),
    'field_department_name_value' => array(
      'id' => 'field_department_name_value',
      'table' => 'node_data_field_department_name',
      'field' => 'field_department_name_value',
    ),
    'field_department_role_value' => array(
      'id' => 'field_department_role_value',
      'table' => 'node_data_field_department_role',
      'field' => 'field_department_role_value',
    ),
    'status' => array(
      'label' => 'Enabled',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'type' => 'yes-no',
      'not' => 0,
      'exclude' => 0,
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'view_node' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => '',
      'exclude' => 0,
      'id' => 'view_node',
      'table' => 'node',
      'field' => 'view_node',
      'relationship' => 'none',
    ),
    'edit_node' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => '',
      'exclude' => 0,
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'relationship' => 'none',
    ),
    'delete_node' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => '',
      'exclude' => 0,
      'id' => 'delete_node',
      'table' => 'node',
      'field' => 'delete_node',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_department_code_value' => array(
      'id' => 'field_department_code_value',
      'table' => 'node_data_field_department_code',
      'field' => 'field_department_code_value',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'cgpa_department' => 'cgpa_department',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'field_department_name_value' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'field_department_name_value_op',
        'identifier' => 'field_department_name_value',
        'label' => 'Department Name',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 0,
      'id' => 'field_department_name_value',
      'table' => 'node_data_field_department_name',
      'field' => 'field_department_name_value',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => '=',
      'value' => 'All',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'operator' => '',
        'identifier' => 'status',
        'label' => 'Published',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'administer cgpa_gradescale grade list',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Administer Department List');
  $handler->override_option('footer_format', '1');
  $handler->override_option('footer_empty', 1);
  $handler->override_option('empty_format', '3');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', 'mini');
  $handler->override_option('use_more', 1);
  $handler->override_option('use_more_always', 0);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'field_faculty_code_value' => 'field_faculty_code_value',
      'field_department_code_value' => 'field_department_code_value',
      'field_department_name_value' => 'field_department_name_value',
      'field_department_role_value' => 'field_department_role_value',
      'status' => 'status',
      'view_node' => 'view_node',
      'edit_node' => 'edit_node',
      'delete_node' => 'delete_node',
    ),
    'info' => array(
      'field_faculty_code_value' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'field_department_code_value' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'field_department_name_value' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'field_department_role_value' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'view_node' => array(
        'separator' => '',
      ),
      'edit_node' => array(
        'separator' => '',
      ),
      'delete_node' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Manage Department List', 'page_1');
  $handler->override_option('path', 'admin/settings/cgpa/departments');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'CGPA Departments',
    'description' => 'Modify department list for the CGPA calculator.',
    'weight' => '0',
    'name' => 'admin',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  return $views;
}
