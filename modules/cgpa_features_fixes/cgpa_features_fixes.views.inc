<?php
/**
 * @file
 * Helper functions for working with views objects and translations.
 */

/**
 * Traverse a View object (depth-first) and look for strings that
 * should be translatable.
 */
function _cgpa_features_fixes_views_strings($thing, $thing_key = NULL, $depth = 0, $parents = array()) {
  $options = array();
  if ($depth > 8) {
    return $options;
  }
  elseif (is_string($thing)) {
    $th = new stdClass();
    $th->val = $thing;
    $th->key = $thing_key;
    $th->parents = $parents;
    $options = array($th);
    return $options;
  }
  else {
    if (is_object($thing)) {
      $keys = array_keys(get_object_vars($thing));
    }
    elseif (is_array($thing)) {
      $keys = array_keys($thing);
    }
    else {
      return $options;
    }

    if ($thing_key != NULL) {
      $parents[] = $thing_key;
    }
    foreach ($keys as $key) {
      if (is_object($thing)) {
        if (isset($thing->$key) && $thing->$key != NULL) {
          $options = array_merge($options, _cgpa_features_fixes_views_strings($thing->$key, $key, $depth + 1, $parents));
        }
      }
      elseif (is_array($thing)) {
        if (!empty($thing[$key])) {
          $options = array_merge($options, _cgpa_features_fixes_views_strings($thing[$key], $key, $depth + 1, $parents));
        }
      }
      else {
        assert(FALSE);
      }
    }
  }
  if ($depth > 0) {
    return $options;
  }
  else {
    $filtered = array_filter(
      $options,
      '_cgpa_features_fixes_views_strings_filter'
    );
    $just_strings = array_map(
      '_cgpa_features_fixes_views_strings_just_strings_map',
      $filtered
    );
    $uniq = array_unique($just_strings);
    return $uniq;
  }
}

/**
 * Fix strings filter
 */
function _cgpa_features_fixes_views_strings_filter($var) {
  $goodkeys = array('label', 'description', 'title', 'header', 'text');
  $conditions = array();
  $conditions[] = is_string($var->key);
  $conditions[] = in_array($var->key, $goodkeys);
  $conditions[] = in_array('display_options', $var->parents);
  $conditions[] = !in_array('style_options', $var->parents);
  $conditions[] = !in_array('view_node', $var->parents);
  $conditions[] = !in_array('delete_node', $var->parents);
  $conditions[] = !in_array('edit_node', $var->parents);
  $conditions[] = !in_array('nid', $var->parents);

  foreach ($conditions as $condition) {
    if (!$condition) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * get strings map
 */
function _cgpa_features_fixes_views_strings_just_strings_map($var) {
  return $var->val;
}
