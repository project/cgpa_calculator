(function($) {

  function calculateCredits(table) {
    var credits = 0;
    table.find('tr:has(td)').each(function() {
      if ($(this).find('input:checked').length > 0) {
        try {
          credits += parseFloat($(this).find('.cgpa-fellowship-credits').text(), 10);
        }
        catch(e) {}
      }
    });

    table.find('.cgpa-fellowship-credits-result').text(credits);
  }

  Drupal.behaviors.cgpaFellowshipCredits = function(context) {
    $('.cgpa-fellowship-credits-table:not(.cgpa-fellowship-credits-table-processed)', context)
      .addClass('cgpa-fellowship-credits-table-processed')
      .find('tr:has(td) input[type="checkbox"]')
      .bind('click', function() {
        var table = $(this).parents('table:first');
        calculateCredits(table);

        table.find('.cgpa-fellowship-cgpa').text('');
      });
  };
})(jQuery);
