<?php
/**
 * @file
 * Module file for CGPA Fellowships module
 */

/**
 * Implements hook_theme().
 */
function cgpa_fellowship_theme() {
  return array(
    'cgpa_fellowship_calculation_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'cgpa_fellowship_calculation_print_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'views_view_field__cgpa_fellowship_calculator_view__field_student_id_value' => array(
      'template' => 'views-view-field--cgpa-fellowship-calculator-view--field-student-id-value',
      'original_hook' => 'views_view_field',
      'preprocess_functions' => array(
        'template_preprocess',
        'template_preprocess_views_view_field',
        'cgpa_fellowship_preprocess_views_view_field__cgpa_fellowship_calculator_view__field_student_id_value',
      ),
      'arguments' => array('view' => NULL, 'options' => NULL, 'row' => NULL),
    ),
  );
}

/**
 * Implements hook_perm().
 */
function cgpa_fellowship_perm() {
  return array(
    'perform fellowship calculations',
  );
}

/**
 * Implements hook_menu().
 */
function cgpa_fellowship_menu() {
  $items = array();

  $items['cgpa-calculator/calculations-admin/%cgpa_fellowship_calculator/fellowship']
    = $items['cgpa-calculator/calculations/%cgpa_fellowship_calculator/fellowship']
      = array(
          'type'              => MENU_LOCAL_TASK,
          'page callback'     => 'drupal_get_form',
          'page arguments'    => array('cgpa_fellowship_calculation_form', 2),
          'access arguments'  => array('perform fellowship calculations'),
          'title'             => 'Calculate Fellowship',
          'weight'            => 1,
        );

  return $items;
}

/**
 * Menu load callback for converting a student id into a list
 * of calculator nodes.
 */
function cgpa_fellowship_calculator_load($student_id) {
  $nodes = array();

   // All the departments the current user is allowed to access.
  $dept_id = cgpa_calculator_get_dept_id();

   // All the faculties the current user is allowed to access.
   $fac_code = cgpa_calculator_get_faculty_list();

  $sql = "
    SELECT n.nid
    FROM {node} n
    LEFT JOIN {content_type_cgpa_calculator} ctcc ON n.vid=ctcc.vid
    WHERE ctcc.field_student_id_value='%s'
  ";

  $result = db_query(db_rewrite_sql($sql), $student_id);

  while ($nid = db_result($result)) {
    if (cgpa_calculator_user_can_view($nid)) {
      $n = node_load($nid);
    }

    if (node_access('view', $n)) {
      $nodes[$nid] = $n;
    }
  }

  $nodes = cgpa_fellowship_array_unique_obj($nodes);

  if ($nodes) {
    return array(
      'id'    => $student_id,
      'nodes' => $nodes,
    );
  }

  return FALSE;
}

/**
 * Making array_unique work with Objects
 */
function cgpa_fellowship_array_unique_obj($array) {
  foreach ($array as $key => $value) {
    if (array_search($value, $array, TRUE) != $key) {
      unset($array[$key]);
    }
  }
  return $array;
}

/**
 * Helper function to set the breadcrumb on fellowship calculator pages.
 */
function cgpa_fellowship_set_breadcrumb($student_info, $admin) {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('CGPA Calculator'), 'cgpa-calculator'),
  );
  if ($admin) {
    $breadcrumb[] = l(t('All Saved Calculations'), 'cgpa-calculator/calculations-admin');
  }
  else {
    $breadcrumb[] = l(t('My Saved Calculations'), 'cgpa-calculator/calculations');
  }
  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Page form callback for 'fellowship/%'
 *
 * @param array $form_state
 *   Form API state
 * @param array $student_info
 *   The cgpa_calculator nodes.
 *
 * @return array
 *   The form array for the form.
 */
function cgpa_fellowship_calculation_form(&$form_state, $student_info) {
  drupal_set_title(t('CGPA Calculations for Student ID @student_id', array('@student_id' => $student_info['id'])));

  $item = menu_get_item();
  cgpa_fellowship_set_breadcrumb($student_info, (strpos($item['path'], 'calculations-admin') !== FALSE));

  $student_id = $student_info['id'];
  $nodes = $student_info['nodes'];

  $ids = isset($form_state['storage']['ids']) ? $form_state['storage']['ids'] : FALSE;
  $print = !empty($form_state['storage']['print']);

  $type = content_types('cgpa_calculator');
  $fields = array();

  foreach ($type['fields'] as $field_name => $field) {
    if ($field['type'] == 'cgpa_calculator_term') {
      $fields[$field_name] = $field;
    }
  }

  $missing = FALSE;
  $terms = array();
  $mark_data = array();
  $data = array();

  $credits = 0;
  $gpa_scaled = 0;

  // Build up form and pass cgpa data.
  $form = array(
    'ids' => array(
      '#tree'       => TRUE,
    ),
    '#action' => '#cgpa-fellowship-calculation-form',
  );

  if ($print) {
    $form['#theme'] = 'cgpa_fellowship_calculation_print_form';
  }

  // Split input on , assuming a comma separated list as input.
  foreach ($nodes as $nid => $node) {

    $form['ids'][$nid]['#node'] = $node;

    // Make a rendered version of the degree field.
    $field_degree = $type['fields']['field_degree'];
    // Hide label.
    $field_degree['display_settings']['label']['format'] = 'none';
    $form['ids'][$nid]['#degree']
      = strip_tags(content_view_field($field_degree, $node));

    // Make rendered version of the institution field.
    $field_institution_name = $type['fields']['field_institution_name'];
    // Hide label.
    $field_institution_name['display_settings']['label']['format'] = 'none';
    $form['ids'][$nid]['#institution']
      = strip_tags(content_view_field($field_institution_name, $node));

    // Make rendered version of the student firstname field.
    $field_student_fname = $type['fields']['field_student_fname'];
    // Hide label.
    $field_student_fname['display_settings']['label']['format'] = 'none';
    $form['ids'][$nid]['#student_fname']
      = content_view_field($field_student_fname, $node);

    // Make rendered version of the student lastname field.
    $field_student_lname = $type['fields']['field_student_lname'];
    // Hide label.
    $field_student_lname['display_settings']['label']['format'] = 'none';
    $form['ids'][$nid]['#student_lname']
      = content_view_field($field_student_lname, $node);

    // Loop over all of the calculation fields on the node.
    foreach ($fields as $field_name => $field) {

      $form['ids'][$nid][$field_name]['#node'] = $node;
      $form['ids'][$nid][$field_name]['#field_name'] = $field_name;
      $form['ids'][$nid][$field_name]['#type_name'] = $node->type;

      $year = $node->{$field_name}['year'];
      $form['ids'][$nid][$field_name]['#year'] = $year;

      // Then go over each term in the year.
      foreach ($node->{$field_name} as $delta => $term) {
        if (!is_numeric($delta)) {
          continue;
        }

        $checked = !empty($ids[$nid][$field_name][$delta]);

        $gradescale = cgpa_calculator_gradescale_nid($node, $field_name);

        $form['ids'][$nid][$field_name]['#mark_data']
          = cgpa_calculator_process_marks($node->{$field_name}, $gradescale);

        // If there is a value, we show it if is is checked off,
        // or if we aren't in print mode.
        if (!cgpa_calculator_content_is_empty($term, $field)) {
          $form['ids'][$nid][$field_name]['#title']
            = $field['widget']['label'] . ' - ' . $year;

          // Calculate the marks for each term individually.
          $tmp = array(&$term);
          $mark_data = cgpa_calculator_process_marks($tmp, $gradescale);

          $term_data = array(
            'term'      => $term,
            'mark_data' => $mark_data,
            'delta'     => $delta,
            'title'     => $node->title,
            'gradescale'  => $gradescale,
          );

          $form['ids'][$nid][$field_name][$delta] = array(
            '#title'          => '',
            '#type'           => $print ? 'hidden' : 'checkbox',
            '#return_value'   => $term['termid'],
            '#default_value'  => $checked,
            '#term_data'      => $term_data,
            '#item'           => $term,
          );

          // If the term has been checked off, use it in CGPA calculation.
          if (!empty($ids[$nid][$field_name][$delta])) {
            if (empty($mark_data['missing'])) {
              $credits += $mark_data['credits'];
              $gpa_scaled += $mark_data['gpa'] * $mark_data['credits'];
            }
            else {
              $missing = TRUE;
            }
          }
        }
      }
    }
  }

  // Build up form and pass cgpa data.
  $form['#cgpa_data']  = array(
    'credits'     => $credits,
    'gpa'         => $missing ? ''  : ($credits ? ($gpa_scaled / $credits) : 0),
  );

  // Add submit button with variable text.
  $form['buttons']['calculate'] = array(
    '#type'     => 'submit',
    '#value'    => ($ids === FALSE) ? t('Calculate') : t('Recalculate'),
  );
  $form['buttons']['print'] = array(
    '#type'     => 'submit',
    '#value'    => t('Print'),
  );

  $form['buttons']['#prefix'] = '<div class="cgpa-fellowship-button-wrapper">';
  $form['buttons']['#suffix'] = '</div>';

  return $form;
}

/**
 * Fellowship calculation form submit callback
 */
function cgpa_fellowship_calculation_form_submit($form, &$form_state) {
  // Remove unselected check boxes.
  $ids = isset($form_state['values']['ids']) ?
    array_filter($form_state['values']['ids']) : array();

  $form_state['rebuild'] = TRUE;
  $form_state['storage']['ids'] = $ids;

  $form_state['storage']['print']
    = ($form['buttons']['print']['#value']
      == $form_state['clicked_button']['#value']);
}

/**
 * Fellowship calculation form theme function
 *
 * Renders a row for each of the terms in the form, along with credit and
 * cgpa data. Also adds JS to autoclear the cgpa on clicking checkboxes, and
 * autocalculation for the number of credits to make life easier for the users.
 *
 */
function theme_cgpa_fellowship_calculation_form($form) {

  drupal_add_js(drupal_get_path('module', 'cgpa_fellowship') . '/cgpa_fellowship.js');
  drupal_add_css(drupal_get_path('module', 'cgpa_fellowship') . '/cgpa_fellowship.css');

  $header = array(
    t('Student Firstname'),
    t('Student Lastname'),
    t('Institution & Degree'),
    t('Year'),
    t('Term'),
    t('Credits'),
    '',
    t('GPA'),
    array(
      'data'  => '',
      // Add class to hide td when printing.
      'class' => 'cgpa-fellowship-checkbox-column',
    ),

  );

  $rows = array();
  foreach (element_children($form['ids']) as $nid) {
    $nodestart = count($rows);
    foreach (element_children($form['ids'][$nid]) as $field) {
      $fieldstart = count($rows);
      foreach (element_children($form['ids'][$nid][$field]) as $delta) {

        $term_data = $form['ids'][$nid][$field][$delta]['#term_data'];
        $rows[] = array(
          'data' => array(
            array(
              'data'  => t('Term ') . ($delta + 1),
            ),
            array(
              'data'  => $term_data['mark_data']['credits'],
              'class' => 'cgpa-fellowship-credits',
              'colspan' => 2,
            ),
            array(
              'data'  => ($term_data['mark_data']['gpa'] !== '') ? number_format($term_data['mark_data']['gpa'], 2) : 'Error',
              'class' => ($term_data['mark_data']['gpa'] !== '') ? '' : 'error',
            ),
            array(
              'data'  => drupal_render($form['ids'][$nid][$field][$delta]),
              'class' => 'cgpa-fellowship-checkbox-column',
            ),
          ),

          // Class used to hide unselected rows in print view.
          'class' => empty($form['ids'][$nid][$field][$delta]['#value']) ? 'cgpa-fellowship-unselected' : '',
        );
      }

      if (isset($rows[$fieldstart])) {
        array_unshift($rows[$fieldstart]['data'], array(
          'data'    => check_plain($form['ids'][$nid][$field]['#title']),
          'rowspan' => count($rows) - $fieldstart,
        ));
      }
    }

    if (isset($rows[$nodestart])) {
      array_splice($rows[$nodestart]['data'], 0, 0, array(
        array(
          'data'    => $form['ids'][$nid]['#student_fname'],
          'rowspan' => count($rows) - $nodestart,
        ),
        array(
          'data'    => $form['ids'][$nid]['#student_lname'],
          'rowspan' => count($rows) - $nodestart,
        ),
        array(
          'data'    => $form['ids'][$nid]['#institution'] . ' - ' . $form['ids'][$nid]['#degree'],
          'rowspan' => count($rows) - $nodestart,
        ),
      ));
    }
  }

  $rows[] = array(
    array(
      'data'    => '',
      'colspan' => 3,
    ),
    array(
      'data'    => '<strong>' . t('Credits:') . '</strong>',
      'align'   => 'right',
    ),
    array(
      'data'    => $form['#cgpa_data']['credits'],
      // Class used for automatic sum of selected credits.
      'class'   => 'cgpa-fellowship-credits-result',
    ),
    array(
      'data'    => '<strong>' . t('CGPA:') . '</strong>',
      'align'   => 'right',
    ),
    array(
      'data'    => ($form['#cgpa_data']['gpa'] !== '') ?
        number_format($form['#cgpa_data']['gpa'], 2) : '',
      // Class used for CGPA clearing on click.
      'class'   => 'cgpa-fellowship-cgpa',
    ),
    array(
      'data'    => '',
      'class'   => 'cgpa-fellowship-checkbox-column',
    ),
  );

  $fellowship_calculation_help_text = '<p>' . t("To calculate a fellowship CGPA for this student, select the terms to be used in the calculation and click the 'Calculate' button below. Clicking 'Print' will generate a printout of the grades for the selected terms as well as the CGPA result.") . '</p>';

  return
    $fellowship_calculation_help_text .
    theme('table', $header, $rows, array('class' => 'cgpa-fellowship-credits-table')) .
    drupal_render($form);

}

/**
 * Theme function for the fellowship form when user presses 'Print.'
 */
function theme_cgpa_fellowship_calculation_print_form($form) {

  drupal_add_js("$(function() { window.print();});", 'inline');
  drupal_add_css(drupal_get_path('module', 'cgpa_fellowship') . '/cgpa_fellowship.css');

  $output = '';

  // Iterate to gather all degrees.
  $degrees = array();
  foreach (element_children($form['ids']) as $nid) {

    // Iterate to gather all years.
    $years = array();
    foreach (element_children($form['ids'][$nid]) as $field_name) {

      // Iterate to gather all items(terms).
      $items = array();

      foreach (element_children($form['ids'][$nid][$field_name]) as $delta) {
        if (is_numeric($delta) && !empty($form['ids'][$nid][$field_name][$delta]['#value'])) {
          $items[$delta] = $form['ids'][$nid][$field_name][$delta]['#item'];
        }
      }

      // Store items if found.
      if ($items) {

        $items['year'] = $form['ids'][$nid][$field_name]['#year'];
        $years[$field_name] = array(
          'items' => $items,
          // Default to False, set True on last degree.
          'last'  => FALSE,
          'mark_data' => $form['ids'][$nid][$field_name]['#mark_data'],
        );
      }
    }

    // Store years if found.
    if ($years) {
      $degrees[$nid] = $years;
    }
  }

  // Mark the very last year as the last one.
  end($degrees);
  if ($last_degree = key($degrees)) {
    end($degrees[$last_degree]);
    $last_year = key($degrees[$last_degree]);
    $degrees[$last_degree][$last_year]['last'] = TRUE;
  }

  // Display all degrees.
  foreach ($degrees as $nid => $degree) {

    // Append each year's table together.
    $years = '';
    foreach ($degree as $field_name => $year) {
      $years .= theme('cgpa_calculator_year_items', $year['items'], $year['mark_data'], $year['last'] ? $form['#cgpa_data'] : FALSE);
    }

    // Append each combo of years and the degree title to the output.
    if ($years) {
      $el = $form['ids'][$nid];
      $output .= '<h3>' . strip_tags($el['#institution'] . ' - ' . $el['#degree']) . '</h3>' . $years;
    }
  }

  return drupal_render($form['info']) . $output . drupal_render($form);
}


/**
 * Implements hook_link()
 */
function cgpa_fellowship_link($type, $object, $teaser = FALSE) {
  $links = array();
  if (is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if ($node->type == 'cgpa_calculator') {
      $links['student_fellowship_calculations'] = array(
        'title' => t('Calculate fellowship CGPA for this student'),
        'href' => 'cgpa-calculator/calculations/' . $node->field_student_id[0]['value'] . '/fellowship',
      );
    }
  }
  return $links;
}
