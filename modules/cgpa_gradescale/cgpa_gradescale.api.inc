<?php
/**
 * @file
 * Provides API functions for requesting gradescale lookups, options,
 * validation and such.
 */

/**
 * Take a grade, and return an array with letter grade and GPA
 *
 * @param integer $nid
 *   nid of grade node.
 * @param string $mark
 *   mark
 *
 * @return array
 *   ('letter' => $letter, 'gpa' => $gpa)
 */
function cgpa_gradescale_mark_convert($nid, $mark) {
  $scale = _cgpa_gradescale_cache_scale($nid);
  $grade_found = FALSE;

  // Counterintuitive, but this is if NOT validate.
  if (cgpa_gradescale_mark_validate($nid, $mark)) {
    return FALSE;
  }

  switch ($scale['type']) {
    case 'range':
      $lbound = -1;
      foreach ($scale['map'] as $bound => $grade) {
        if ($bound > $lbound && $bound <= $mark) {
          $lbound = $bound;
          $grade_found = $grade;
        }
      }
      break;
    case 'match':
    case 'select':
      $grade_found = isset($scale['map'][$mark]) ? $scale['map'][$mark] : FALSE;
      break;
    case '':
      // Missing gradescale error already shown.
      return FALSE;
  }

  $grades = cgpa_gradescale_load_grades();
  if ($grade_found !== FALSE) {
    if (isset($grades[$grade_found])) {
      $grade_gpa = $grades[$grade_found];
      return array(
        'letter'  => $grade_found,
        'gpa'     => $grade_gpa,
      );
    }
    else {
      // Mark exists but could not be converted because the grade is missing.
      cgpa_gradescale_grade_lookup_error();
    }
  }

  // Mark does not exist in gradescale.
  return FALSE;
}

/**
 * Validate a mark based on a scale option.
 *
 * @param integer $nid
 *   grade node nid
 * @param string $mark
 *   mark
 *
 * @return string
 *   NULL if valid, otherwise a message.
 */
function cgpa_gradescale_mark_validate($nid, $mark) {
  // No validation needs to be done for a select, since FAPI
  // should enforce a valid option.
  $scale = _cgpa_gradescale_cache_scale($nid);
  if ($scale['type'] == 'select') {
    return;
  }

  switch ($scale['type']) {
    case 'range':
      if (!is_numeric($mark) || $mark < 0) {
        return t("The mark must be a positive number.");
      }
      if ($mark > $scale['max']) {
        return t("The mark is greater than the maximum @max.", array('@max' => $scale['max']));
      }
      $min = 0;
      foreach ($scale['map'] as $bound => $gnid) {
        if ($bound <= $mark) {
          return;
        }
        $min = min($min, $bound);
      }
      // The mark is bellow the lowest entered bound.
      return t("The mark must be at least @min.", array('@min' => $min));
    case 'match':
      if (isset($scale['map'][$mark])) {
        return;
      }
      return t("Mark '@mark' is not valid for this gradescale.", array('@mark'  => $mark));
  }
}

/**
 * Return an array of grade options.
 *
 * @param integer $nid
 *   grade node nid.
 *
 * @return array
 *   options.
 */
function cgpa_gradescale_mark_options($nid) {
  $scale = _cgpa_gradescale_cache_scale($nid);
  if ($scale['type'] == 'select') {
    return drupal_map_assoc(array_keys($scale['map']));
  }

}

/**
 * Caches the scale
 */
function _cgpa_gradescale_cache_scale($nid) {
  static $scales = array();
  if (!isset($scales[$nid])) {
    if ($scale = node_load($nid)) {
      $type = $scale->field_scale_type[0]['value'];
      $max = NULL;
      if ($type == 'range') {
        $max = $scale->field_scale_max[0]['value'];
      }
      $map = array();
      foreach ($scale->field_scale_scale as $pair) {
        $map[$pair['transcript_grade']] = $pair['grade'];
      }
      $scales[$nid] = array('type' => $type, 'map' => $map, 'max' => $max);
    }
    else {
      $scales[$nid] = array('type' => '', 'map' => array());

      drupal_set_message(t('Error: The grade scale with node id @nid could not be found.', array('@nid' => $nid)), 'error');
    }
  }
  return $scales[$nid];
}
