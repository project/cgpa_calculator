<?php

/**
 * @file
 * Provides cgpa_import with the classes it needs for import.
 */

module_load_include('inc', 'node', 'node.pages');

class ScaleType {
  private $map = array(
    'Range' => 'range',
    'Exact Value' => 'select',
    'Descriptive' => 'select',
  );
  private $type;

  /**
   * Constructor.
   */
  public function __construct($type) {
    if (isset($this->map[$type])) {
      $this->type = $type;
    }
    else {
      throw new Exception("Invalid type: " . $type);
    }
  }

  /**
   * Returns the type.
   */
  public function getType() {
    return $this->map[$this->type];
  }

  /**
   * Returns the type.
   */
  public function printType() {
    return $this->type;
  }
}

class Institution {
  private $name;
  private $country;

  /**
   * Constructor.
   */
  public function __construct($country, $name) {
    $this->name = $name;
    $this->country = $country;
  }

  /**
   * Returns the country.
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * Returns the name.
   */
  public function getName() {
    return $this->name;
  }

}

class Institutions {
  private $list;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->list = array();
  }

  /**
   * Takes a country and institution name and returns an institution.
   */
  public function institution($country, $name) {
    $cid = $country->getId();
    if (isset($list[$cid])) {
      if (!isset($list[$cid][$name])) {
        $institution = new Institution($country, $name);
        $list[$cid][$name] = $institution;
      }
      else {
        $institution = $list[$cid][$name];
      }
    }
    else {
      $institution = new Institution($country, $name);
      $list[$cid] = array(
        $name => $institution,
      );
    }
    return $institution;
  }
}

class GradeScale {
  private $map;
  private $institution;
  private $type;
  private $description;
  private $max;

  /**
   * Constructor.
   */
  public function __construct($id, $institution, $type, $description) {
    $this->institution = $institution;
    $this->id = $id;
    $this->type = $type;
    $this->description = $description;
    $this->map = array();
    if ($type->getType() == 'range') {
      $this->max = 0;
    }
    else {
      $this->max = NULL;
    }
  }

  /**
   * Adds a grade.
   */
  public function addGrade($input_grade, $output_grade) {
    if ($this->type->getType() == 'range') {
      $bounds = explode('-', $input_grade);
      $this->map[$bounds[0]] = $output_grade;
      if (isset($bounds[1])) {
        $this->max = max($bounds[1], $this->max);
      }
      else {
        $this->max = max($bounds[0], $this->max);
      }
    }
    else {
      $this->map[$input_grade] = $output_grade;
    }
  }

  /**
   * Make it a node.
   */
  public function toNode() {
    // Check whether we need to update or create.
    $query = 'SELECT n.nid FROM {node} n INNER JOIN {content_type_cgpa_gradescale} ctgg ON ctgg.vid = n.vid WHERE field_scale_id_value = %d';
    if ($nid = db_result(db_query($query, array($this->id)))) {
      $node = node_load($nid);
      // Let node autotitle update the title.
      $node->title = NULL;
    }
    else {
      $node = new StdClass();
      $node->type = 'cgpa_gradescale';
      $node->title = NULL;
      $node->nid = NULL;
      $node->language = NULL;
      node_object_prepare($node);
    }
    $node->field_scale_id = array(
      0 => array(
        'value' => $this->id,
      ),
    );
    $country = $this->institution->getCountry();
    $node->field_scale_country = array(
      0 => array(
        'value' => $country->getId(),
      ),
    );
    $node->field_scale_name = array(
      0 => array(
        'value' => $this->institution->getName(),
      ),
    );
    $node->field_scale_type = array(
      0 => array(
        'value' => $this->type->getType(),
      ),
    );
    $node->field_scale_max = array(
      0 => array(
        'value' => $this->max,
      ),
    );
    $node->field_scale_scale = array();
    foreach ($this->map as $transcript => $normalized) {
      $node->field_scale_scale[] = array(
        'transcript_grade' => $transcript,
        'grade' => $normalized,
      );
    }
    return $node;
  }
}

class GradeScaleImporter extends Importer {
  // CSV column numbers.
  const COL_ID = 0;
  // Country.
  const COL_CRY = 1;
  const COL_NAME = 2;
  const COL_TYPE = 3;
  // Empty col 4.
  // First row of each scale.
  const COL_DESC = 5;
  // Next rows.
  const COL_TRAN = 5;
  const COL_GRADE = 6;
  const COL_GPA = 7;

  protected $state;
  protected $gradescale;
  protected $institutions;
  protected $countries;

  /**
   * Constructor.
   */
  public function __construct($filename) {
    parent::__construct($filename);
    $this->institutions = new Institutions();
    $this->countries = cgpa_country_get_countries();
  }

  /**
   * Import a row.
   *
   * @return: FALSE when last row parsed, TRUE otherwise.
   */
  protected function parse() {
    $end = FALSE;
    $row = $this->csv->getRow();
    if (!$row) {
      if ($this->state == self::STATE_STARTED) {
        // Process last gradecale.
        $row = array();
      }
      $end = TRUE;
    }
    // Check for an empty row.
    if (!array_reduce($row, '_cgpa_gradescale_has_content', FALSE)) {
      if ($this->state == self::STATE_STARTED) {
        $node = $this->gradescale->toNode();
        node_save($node);
      }
      $this->state = self::STATE_NEW;
    }
    elseif ($this->state == self::STATE_NEW) {
      // No ID column in CSV.
      if (count($row) == 7) {
        array_unshift($row, -1);
      }
      if (is_numeric($row[self::COL_ID])) {
        try {
          $id = $row[self::COL_ID];
          // No IDs.
          if ($id < 0) {
            $id = NULL;
          }
          $country_names = $this->countryNames($row[self::COL_CRY]);
          $country = $this->countries->getCountryByName($country_names['en'], 'en');
          if ($country === FALSE) {
            throw new Exception(t("Could not find country matching @name.", array('@name' => $country_names['en'])));
          }
          $institution = $this->institutions->institution($country, $row[self::COL_NAME]);
          $type = new ScaleType($row[self::COL_TYPE]);
          $desc = $row[self::COL_DESC];
          $this->gradescale = new GradeScale($id, $institution, $type, $desc);
          $this->state = self::STATE_STARTED;
        }
        catch (Exception $e) {
          $mess = t("Failed to start new gradescale on row:") . "\n\"";
          $mess .= implode(', ', $row);
          $mess .= "\".\n" . $e->getMessage() . "\n";
          throw new Exception($mess);
        }
      }
    }
    else {
      $this->gradescale->addGrade($row[self::COL_TRAN], $row[self::COL_GRADE]);
    }
    return !$end;
  }

  /**
   * Split "english/french" country names.
   */
  protected function countryNames($country_names) {
    if (strpos($country_names, '/') !== FALSE) {
      $country_names = array_map('trim', explode('/', $country_names, 2));
      $names = array(
        'en' => $country_names[0],
        'fr' => $country_names[1],
      );
    }
    else {
      $names = array(
        'en' => trim($country_names),
        'fr' => trim($country_names),
      );
    }
    return $names;
  }
}

/**
 * Return if we have content or not
 */
function _cgpa_gradescale_has_content($v1, $v2) {
  return $v1 || ($v2 !== '');
}
