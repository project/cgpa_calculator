<?php

/**
 * @file
 * Content types for the cgpa_gradescale module.
 */

/**
 * Implementation of hook_content_default_fields().
 */
function cgpa_gradescale_content_default_fields() {
  $fields = array();

  // Exported field: field_scale_country
  $fields['cgpa_gradescale-field_scale_country'] = array(
    'field_name' => 'field_scale_country',
    'type_name' => 'cgpa_gradescale',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '',
    'max' => '',
    'allowed_values' => '',
    'allowed_values_php' => 'return cgpa_country_get_list();',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Country',
      'weight' => '-3',
      'description' => '',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_scale_id
  $fields['cgpa_gradescale-field_scale_id'] = array(
    'field_name' => 'field_scale_id',
    'type_name' => 'cgpa_gradescale',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '0',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '',
    'max' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scale_id][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'ID',
      'weight' => '-4',
      'description' => 'Required to sync with imports.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_scale_max
  $fields['cgpa_gradescale-field_scale_max'] = array(
    'field_name' => 'field_scale_max',
    'type_name' => 'cgpa_gradescale',
    'display_settings' => array(
      'weight' => '1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_float',
    'required' => '0',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '',
    'max' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scale_max][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Grade Max',
      'weight' => '1',
      'description' => 'Maximum possible grade when entering a numeric range grade.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_scale_name
  $fields['cgpa_gradescale-field_scale_name'] = array(
    'field_name' => 'field_scale_name',
    'type_name' => 'cgpa_gradescale',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scale_name][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Grade Scale Name',
      'weight' => '-2',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_scale_scale
  $fields['cgpa_gradescale-field_scale_scale'] = array(
    'field_name' => 'field_scale_scale',
    'type_name' => 'cgpa_gradescale',
    'display_settings' => array(
      'weight' => '2',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'cgpa_gradescale_scale',
    'required' => '1',
    'multiple' => '1',
    'module' => 'cgpa_gradescale',
    'active' => '1',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'transcript_grade' => '',
          'grade_nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Grade Scale',
      'weight' => '2',
      'description' => 'If you are creating a \'Numeric Range\' grade scale, enter the lower bound of the numeric range as the transcript grade. The next highest transcript grade will be used as the upper bound. For the highest transcript grade entered, the \'grade max\' will be used as the upper bound.',
      'type' => 'cgpa_gradescale_scale',
      'module' => 'cgpa_gradescale',
    ),
  );

  // Exported field: field_scale_type
  $fields['cgpa_gradescale-field_scale_type'] = array(
    'field_name' => 'field_scale_type',
    'type_name' => 'cgpa_gradescale',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => 'return array(
\'range\' => t(\'Numeric Range\'),
\'select\' => t(\'Exact Match (Select Values)\'),
\'match\' => t(\'Exact Match (Manual Entry)\')
);
',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Grade Type',
      'weight' => 0,
      'description' => 'To create a grade scale based on a set of numeric grade ranges (i.e. 80-100, 70-80, etc.) use \'Numeric Range\'. To use an exact value (i.e. A+, A, B, etc.) use \'Exact Value\'. 

The Exact Value (Select Values) option will allow you to choose the transcript grades from a select box. The Exact Value (Manually Entry) option will require you to enter the values from the transcript manually. ',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Country');
  t('Grade Max');
  t('Grade Scale');
  t('Grade Scale Name');
  t('Grade Type');
  t('ID');

  return $fields;
}
