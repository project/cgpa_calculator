<?php

/**
 * @file
 * Features for the cgpa_gradescale module.
 */
 
/**
 * Implementation of hook_ctools_plugin_api().
 */
function cgpa_gradescale_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function cgpa_gradescale_node_info() {
  $items = array(
    'cgpa_gradescale' => array(
      'name' => t('CGPA Grade Scale'),
      'module' => 'features',
      'description' => t('Manage grading scales available for each country and the conversion to local grades.	'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '1',
      'body_label' => t('Grade Scale Notes'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function cgpa_gradescale_views_api() {
  return array(
    'api' => '2',
  );
}
