<?php
/**
 * @file
 * Defines module schema and provides hooks for installing and updating schema.
 */

/**
 * Implements hook_schema().
 */
function cgpa_gradescale_schema() {
  $tables = array();

  $tables['cgpa_gradescale_grades'] = array(
    'description' => 'Stores grade/gpa pairs for the cgpa_gradescale module.',
    'fields'      => array(
      'grade'       => array(
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'gpa' => array(
        'type'        => 'numeric',
        // 10 significant digits.
        'precision'   => 10,
        // 2 decimal places.
        'scale'       => 2,
      ),
    ),
    'primary key' => array('grade'),
  );

  return $tables;
}

/**
 * Implements hook_install().
 */
function cgpa_gradescale_install() {
  drupal_install_schema('cgpa_gradescale');
}

/**
 * Implements hook_uninstall().
 */
function cgpa_gradescale_uninstall() {
  drupal_uninstall_schema('cgpa_gradescale');
}

/**
 * Implements hook_update_N().
 *
 * Installs the newly added grades table.
 */
function cgpa_gradescale_update_6000() {
  drupal_install_schema('cgpa_gradescale');

  return array();
}
