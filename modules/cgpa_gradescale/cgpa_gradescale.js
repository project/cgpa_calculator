(function($) {

  Drupal.behaviors.cgpa_gradescale = function (context) {
    // Only show the gradescale max value if we have a numeric range.
    $('#edit-field-scale-type-value:not(.cgpa-gradescale-max-processed)', context)
      .addClass('cgpa-gradescale-max-processed')
      .change(function() {
        if ($(this).val() == 'range') {
          $('#edit-field-scale-max-0-value-wrapper', context).show();
        }
        else {
          $('#edit-field-scale-max-0-value-wrapper', context).hide();
        }
      })
      .change();

    $('#field-scale-scale-items .tabledrag-handle', context).attr('tabIndex', -1);
  };
})(jQuery);
