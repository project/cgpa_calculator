<?php

/**
 * @file
 * Strongarm settings for the cgpa_gradescale module.
 */

/**
 * Implementation of hook_strongarm().
 */
function cgpa_gradescale_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_cgpa_gradescale';
  $strongarm->value = '1';
  $export['ant_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_cgpa_gradescale';
  $strongarm->value = '[field_scale_country-formatted] - [field_scale_name-raw] - [field_scale_type-formatted]';
  $export['ant_pattern_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_cgpa_gradescale';
  $strongarm->value = 0;
  $export['ant_php_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_cgpa_gradescale';
  $strongarm->value = 0;
  $export['comment_anonymous_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_cgpa_gradescale';
  $strongarm->value = '0';
  $export['comment_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_cgpa_gradescale';
  $strongarm->value = '3';
  $export['comment_controls_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_cgpa_gradescale';
  $strongarm->value = '4';
  $export['comment_default_mode_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_cgpa_gradescale';
  $strongarm->value = '1';
  $export['comment_default_order_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_cgpa_gradescale';
  $strongarm->value = '50';
  $export['comment_default_per_page_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_cgpa_gradescale';
  $strongarm->value = '0';
  $export['comment_form_location_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_cgpa_gradescale';
  $strongarm->value = '1';
  $export['comment_preview_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_cgpa_gradescale';
  $strongarm->value = '1';
  $export['comment_subject_field_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_cgpa_gradescale';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'revision_information' => '4',
    'author' => '5',
    'options' => '6',
    'menu' => '3',
  );
  $export['content_extra_weights_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_cgpa_gradescale';
  $strongarm->value = 1;
  $export['enable_revisions_page_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_cgpa_gradescale';
  $strongarm->value = '0';
  $export['language_content_type_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_cgpa_gradescale';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_cgpa_gradescale';
  $strongarm->value = 0;
  $export['show_diff_inline_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_cgpa_gradescale';
  $strongarm->value = 1;
  $export['show_preview_changes_cgpa_gradescale'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_cgpa_gradescale';
  $strongarm->value = '1';
  $export['upload_cgpa_gradescale'] = $strongarm;

  return $export;
}
