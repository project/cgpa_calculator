<?php
/**
 * @file
 * The drush command file for commands importing gradescale and country data.
 */

/**
 * Implements hook_drush_help().
 */
function cgpa_import_drush_help($command) {
  switch ($command) {
    case 'drush:cgpa-import-gradescale':
      return dt('Import a gradescale. The input should be a csv.');
    case 'drush:cgpa-import-countries':
      return dt('Import a list of countries. The input should be a csv.');
  }
}

/**
 * Implements hook_drush_command().
 */
function cgpa_import_drush_command() {
  $items = array();
  $items['cgpa-import-gradescale'] = array(
    'description' => dt('Import a gradescale csv.'),
    'arguments' => array(
      'filename' => dt('Gradescales csv.'),
    ),
    'examples' => array(
      'Example' => 'drush cgpa-import-gradescale gradescales.csv',
    ),
    'aliases' => array('cig'),
  );
  $items['cgpa-import-countries'] = array(
    'description' => dt('Import a countries csv.'),
    'arguments' => array(
      'filename' => dt('countries csv.'),
    ),
    'examples' => array(
      'Example' => 'drush cgpa-import-countries countries.csv',
    ),
    'aliases' => array('cic'),
  );

  return $items;
}

/**
 * callback for cgpa-import-gradescale.
 */
function drush_cgpa_import_gradescale($filename = NULL) {
  if (!isset($filename)) {
    drush_set_error(dt('Please provide a gradescale csv.'));
    return;
  }
  module_load_include('inc', 'cgpa_import', 'cgpa_import.import');
  module_load_include('inc', 'cgpa_gradescale', 'cgpa_gradescale.cgpa_import');
  $importer = new GradeScaleImporter($filename);
  $errors = $importer->import();
  if (!empty($errors)) {
    drush_set_error('CGPA_IMPORT_ERROR', dt('Some rows failed to be imported.'));
    foreach ($errors as $error) {
      drush_log($error, 'warning');
    }
    drush_log(dt('Done.'));
  }
  else {
    drush_log(dt('Done.'), 'success');
  }
}

/**
 * callback for cgpa-import-countries.
 */
function drush_cgpa_import_countries($filename = NULL) {
  if (!isset($filename)) {
    drush_set_error(dt('Please provide a countries list csv.'));
    return;
  }
  module_load_include('inc', 'cgpa_import', 'cgpa_import.import');
  module_load_include('inc', 'cgpa_country', 'cgpa_country.cgpa_import');
  $importer = new CountryImporter($filename);
  $errors = $importer->import();
  if (!empty($errors)) {
    drush_set_error('CGPA_IMPORT_ERROR', dt('Some rows failed to be imported.'));
    foreach ($errors as $error) {
      drush_log($error, 'warning');
    }
    drush_log(dt('Done.'));
  }
  else {
    drush_log(dt('Done.'), 'success');
  }
}
