<?php
/**
 * @file
 * Classes for use whem importing.
 */

class CSV {
  private $fp;

  /**
   * Constructor
   */
  public function __construct($filename, $mode = 'r') {
    $this->fp = fopen($filename, $mode);
  }

  /**
   * Gets a CSV line.
   */
  public function getRow() {
    return fgetcsv($this->fp);
  }

  /**
   * Closes the file.
   */
  public function close() {
    return @fclose($this->fp);
  }

  /**
   * Destructor
   */
  public function __destruct() {
    $this->close();
  }
}

abstract class Importer {
  const STATE_NEW = 0;
  const STATE_STARTED = 1;

  protected $csv;

  /**
   * Constructor
   */
  public function __construct($filename) {
    $this->csv = new CSV($filename);
    // Remove headers.
    $this->csv->getRow();
    $this->state = self::STATE_NEW;
  }

  /**
   * Import a row.
   *
   * @return: FALSE when last row parsed, TRUE otherwise.
   */
  abstract protected function parse();

  /**
   * Does the import.
   */
  public function import() {
    $exceptions = array();
    while (1) {
      try {
        if (!$this->parse()) {
          break;
        }
      }
      catch (Exception $e) {
        $exceptions[] = $e->getMessage();
      }
    }
    return $exceptions;
  }
}
