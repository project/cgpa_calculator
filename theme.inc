<?php
/**
 * @file
 * Defines theme functions for the cgpa_calculator field widget and formatters.
 */

/**
 * Custom #type theme function for the nodereference_url
 */
function theme_cgpa_calculator_gradescale_preview($element) {
  if (($nid = $element['#value']) && ($node = node_load($nid)) && ($node->type == 'cgpa_gradescale')) {

    $gradescale_table = theme(
      'cgpa_gradescale_table',
      $node,
      $node->field_scale_scale) . check_markup($node->body, $node->format);

    /*
     * Remove table sticky-headers to avoid weird rendering on calculator
     * page when embedded in fieldset in IE7.
     * The add some conditional CSS to make table render properly when expanded.
     * http://drupal.org/node/247957
     */
    $gradescale_table = preg_replace('/(<table[^>]*)sticky-enabled([^>]*>)/', '\1\2', $gradescale_table) .
      '<!--[if IE 7]><style type="text/css">' . "
      fieldset.group-grade-scale fieldset.collapsed table * {
         display: inline;
      }
      " . '</style><![endif]-->';


    $fieldset = array(
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
      '#children'     => $gradescale_table,
      '#title'        => t('Grade Scale Information'),
    );

    return theme('nodereference_url', $element) . drupal_render($fieldset);
  }
  else {
    return theme('nodereference_url', $element);
  }
}

/**
 * #type theme function for the term_set widget element.
 *
 * Does not have to do much because all rendering is done in the element #theme.
 */
function theme_cgpa_calculator_term_set($elements) {
  drupal_add_js(drupal_get_path('module', 'cgpa_calculator') . '/cgpa_calculator.js');
  if (empty($elements['#field']['widget']['collapse'])) {
    return '<h2>' . $elements['#title'] . '</h2>' . $elements['#children'];
  }
  else {
    $fieldset = array(
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#collapsed'    => !$elements['#has_mark_values'],
      '#children'     => $elements['#children'],
      '#title'        => $elements['#title'],
    );
    return drupal_render($fieldset);
  }
}

/**
 * #theme function for the term_set widget element.
 *
 * Renders all of the child form elements into a table along with all of
 * the converted gpa grade values, and renders the calculated GPA and CGPA.
 */
function theme_cgpa_calculator_term_set_table($element) {
  $yearfield = '<div class="' . $element['#type_name'] . '-field-year">' . drupal_render($element['year']) . '</div>';
  unset($element['year']);
  $header = array(
    t('Term'),
    t('Transcript Grade'),
    t('Credits'),
    t('Equivalent Grade'),
    t('Equivalent Grade Points'),
    t(''),
  );

  drupal_alter('cgpa_calculator_table_headers', $header);

  $rows = array();
  $row_start = 0;

  // Each Term item.
  foreach (element_children($element) as $key) {
    if (!is_numeric($key)) {
      continue;
    }
    $term = $element[$key];

    // Each value.
    foreach (element_children($term['value']) as $termkey) {
      if ($termkey === '_add_more') {
        continue;
      }

      $value_element = $term['value'][$termkey];
      $value = empty($element['#marks'][$key]['value'][$termkey]) ?
        array() : $element['#marks'][$key]['value'][$termkey];

      $error = (!isset($value['convmark']) || !isset($value['gpa'])) &&
        (isset($value['mark']) &&
        ($value['mark'] !== '' ||
        $value['credits'] !== ''));

      $row = array(
        array(
          'data'  => drupal_render($value_element['mark']),
          'class' => 'cgpa-calculator-term-set-mark',
        ),
        array(
          'data'  => drupal_render($value_element['credits']),
          'class' => 'cgpa-calculator-term-set-credits',
        ),
        array(
          'data'  => $error ? 'Error' : (isset($value['convmark']) ? check_plain($value['convmark']) : ''),
          'class' => 'cgpa-calculator-term-set-info' . ($error ? ' error' : ''),
        ),
        array(
          'data'  => $error ? 'Error' : (isset($value['gpa']) ? check_plain(number_format($value['gpa'], 2)) : ''),
          'class' => 'cgpa-calculator-term-set-info' . ($error ? ' error' : ''),
          'colspan' => 2,
        ),
      );

      $rows[] = $row;
    }

    // Define the row for "Add another grade" button.
    $rows[] = array(
      array(
        'data'  => drupal_render($term['value']['_add_more']),
        'colspan' => '5',
      ),
    );

    // Define the cell for the term label, and
    // append it to the first row in this section.
    array_unshift($rows[$row_start], array(
      'data'  => t('Term !num', array('!num' => ($key + 1))) .
      drupal_render($element['term']) .
      drupal_render($element[$key]['term']),
      'rowspan' => count(element_children($term['value'])),
    ));

    $row_start = count($rows);
  }

  $rows = array_merge($rows, cgpa_calculator_gpa_rows($element['#mark_data']));

  array_push($rows[count($rows) - 1], array(
    'data'  => drupal_render($element['_update']),
  ));
  $table
    = theme('table', $header, $rows, array('class' => 'cgpa-calculator-term-set-table')) .
    '<div class="description">' . $element['#description'] . '</div>';
  // Disable sticky headers. Sticky headers break the form input for year in
  // the header. There is no setting to disable sticky headers, so we need to
  // remove it from the printed version.
  $table = preg_replace('/ ?sticky-enabled/', '', $table);
  return $yearfield . $table;
}

/**
 * One-table field formatter for the field.
 */
function theme_cgpa_calculator_formatter_onetable($element) {

  $field = content_fields($element['#field_name'], $element['#type_name']);

  $gradescale = $element['#node']->{$field['gradescale']}[0]['nid'];

  $items = array();
  $items['year'] = $element['year']['#item'];
  $value_count = 0;
  foreach (element_children($element) as $key) {
    if (!is_numeric($key)) {
      continue;
    }

    $items[$key] = $element[$key]['#item'];

    if (isset($items[$key]['value'])) {
      $value_count += count($items[$key]['value']);
    }
  }
  cgpa_calculator_strip_buttons($items);

  if ($value_count === 0) {
    return FALSE;
  }

  $mark_data = cgpa_calculator_process_marks($items, $gradescale);
  $cgpa_data
    = cgpa_calculator_process_cgpa(
        $field, (array) $element['#node'], $gradescale);

  return theme('cgpa_calculator_year_items', $items, $mark_data, $cgpa_data);
}

/**
 * Helper theme function to take a list of items, and optional mark_data
 * and optionally cgpa_data.
 *
 * @param array $items
 *   A list of term items such as those returned by the field.
 *
 * @param array $mark_data
 *   The GPA and credit data.
 *   @see cgpa_calculator_gpa_rows
 *
 * @param array $cgpa_data
 *   The CGPA and credit data.
 *   @see cgpa_calculator_gpa_rows
 *
 */
function theme_cgpa_calculator_year_items($items, $mark_data = FALSE, $cgpa_data = FALSE) {

  $year = $items['year'];
  unset($items['year']);

  $header = array(
    $year !== FALSE ? check_plain($year) : '',
    t('Transcript Grade'),
    t('Credits'),
    t('Equivalent Grade'),
    t('Equivalent Grade Points'),
  );

  drupal_alter('cgpa_calculator_table_headers', $header);

  $rows = array();
  foreach ($items as $key => $item) {

    if (is_numeric($key) && !empty($item['value']) && is_array($item)) {
      $rowstart = count($rows);

      foreach ($item['value'] as $delta => $value) {
        if ($value['mark'] === '') {
          continue;
        }

        $row = array(
          array(
            'data'  => $value['mark'],
            'class' => 'cgpa-calculator-term-popup-mark',
          ),
          array(
            'data'  => $value['credits'],
            'class' => 'cgpa-calculator-term-popup-credits',
          ),
          array(
            'data'  => isset($value['convmark']) ? check_plain($value['convmark']) : 'Error',
            'class' => isset($value['convmark']) ? '' : 'error',
          ),
          array(
            'data'  => isset($value['gpa']) ? number_format($value['gpa'], 2) : 'Error',
            'class' => isset($value['gpa']) ? '' : 'error',
          ),
        );

        $rows[] = $row;
      }

      if (isset($rows[$rowstart])) {
        array_unshift($rows[$rowstart], array(
          'data'  => t('Term !num', array('!num' => ($key + 1))),
          'rowspan' => count($rows) - $rowstart,
        ));
      }
    }
  }

  if (count($rows) > 0) {
    if ($mark_data) {
      $rows = array_merge($rows, cgpa_calculator_gpa_rows($mark_data, $cgpa_data));
    }
    $table = theme('table', $header, $rows);

    // Remove Sticky headers because tables are small and the headers 
    // are showing weirdly when the page is printed.
    $table = preg_replace('/ ?sticky-enabled/', '', $table);
    return $table;
  }
  return FALSE;
}

/**
 * Helper function to build 2 table rows containing the cgpa data,
 * to be appended to the formatter table and the widget view table.
 *
 * @param array $mark_data
 *   GPA data. Format:
 *     array(
 *       'credits'  => 4,
 *       'gpa'      => 3.5,
 *     )
 *
 * @param array $cgpa_data
 *   [optional]
 *   CGPA Data. Format:
 *     array(
 *       'credits'  => 8,
 *       'gpa'      => 3.8,
 *     )
 */
function cgpa_calculator_gpa_rows($mark_data, $cgpa_data = FALSE) {
  $rows = array();

  $rows[] = array(
    array(
      'data' => '',
    ),
    array(
      'data'  => '<strong>' . t('Credits:') . '</strong>',
      'align' => 'right',
    ),
    array(
      'data'  => $mark_data['credits'],
    ),
    array(
      'data'  => '<strong>' . t('GPA:') . '</strong>',
      'align' => 'right',
    ),
    array(
      'data'  => ($mark_data['gpa'] !== '') ? number_format($mark_data['gpa'], 2) : '',
    ),
  );

  if ($cgpa_data !== FALSE) {
    $rows[] = array(
      array(
        'data' => '',
      ),
      array(
        'data'  =>  '<strong>' . t('Credits (overall):') . '</strong>',
        'align' => 'right',
      ),
      array(
        'data'  => $cgpa_data['credits'],
      ),
      array(
        'data'  => '<strong>' . t('CGPA:') . '</strong>',
        'align' => 'right',
      ),
      array(
        'data'  => ($cgpa_data['gpa'] !== '') ? number_format($cgpa_data['gpa'], 2) : '',
      ),
    );
  }

  return $rows;
}
