
<div class='field'>
  <div class='field-label-inline-first'><?php print t('Credits') . ':'; ?></div>
  <?php print $element['#mark_data']['credits']; ?>
</div>
<div class='field'>
  <?php $error = ($element['#mark_data']['gpa'] === ''); ?>
  <div class='field-label-inline-first'><?php print t('Overall CGPA') . ':'; ?></div>
  <span class="<?php if ($error) print 'error'; ?>"><?php print $error ? 'Error' : number_format($element['#mark_data']['gpa'], 2); ?></span>
</div>
<div class='field'>
  <?php $error_2yr = ($element['#mark_data_2yr']['gpa'] === ''); ?>
  <div class='field-label-inline-first'><?php print t('CGPA (last 2 years)') . ':'; ?></div>
  <span class="<?php if ($error_2yr) print 'error'; ?>"><?php print $error_2yr ? 'Error' : number_format($element['#mark_data_2yr']['gpa'], 2); ?></span>
</div>
