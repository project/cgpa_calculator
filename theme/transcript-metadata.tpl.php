
<div class='field'>
  <div class='field-label-inline-first'><?php print t('Date Created') . ':'; ?></div>
  <?php print $element['#created']; ?>
</div>

<div class='field'>
  <div class='field-label-inline-first'><?php print t('Date Last Changed') . ':'; ?></div>
  <?php print $element['#changed']; ?>
</div>

<div class='field'>
  <div class='field-label-inline-first'><?php print t('Date Printed') . ':'; ?></div>
  <?php print $element['#now']; ?>
</div>

<div class='field'>
  <div class='field-label-inline-first'><?php print t('Created By') . ':'; ?></div>
  <?php print $element['#username']; ?>
</div>
